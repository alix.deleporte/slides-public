\documentclass[mathserif]{beamer} \usetheme{Boadilla}
% \usepackage[francais]{babel}
\usepackage[utf8]{inputenc} % Uses the utf8 input encoding
\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs
\usepackage[style=authoryear,backend=biber]{biblatex}
\addbibresource{main.bib} \usepackage{etoolbox} \makeatletter
\patchcmd{\@@description}{\advance\beamer@descdefault by
  \labelsep}{\advance\beamer@descdefault by -1em}{}{} \makeatother

\usepackage{calc} \usepackage{xcolor}

% \AtBeginSection[] { \setbeamercolor{section in toc}{fg=alerted
% text.fg} \setbeamercolor{section in toc
% shaded}{bg=structure!20,fg=structure} \setbeamertemplate{section in
% toc shaded}[default][100]
% \begin{frame}<beamer>
%   \frametitle{Outline}
%   \tableofcontents[currentsection,hideallsubsections]
% \end{frame}
% }

\definecolor{myorange}{RGB}{180,90,0}

\definecolor{mygreen}{RGB}{70,140,0}

\newcommand{\mycite}[1]{{\color{mygreen} \small #1}}
\newcommand{\myemph}[1]{{\color{myorange} #1}}

\usepackage[nomath]{kpfonts} \usepackage{eulervm}
% \usepackage{default}

\input{thm-config}

\title[Imaginary Schrödinger and Mabuchi]{Imaginary time quantum
  evolution and geodesics in the space of Kähler metrics}
\date{November 08, 2022}
\author{Alix Deleporte}
\institute[LMO]{Laboratoire de Mathématiques
  d'Orsay\\Université Paris-Saclay} \newcommand{\spline}{\hline}
\renewcommand{\arraystretch}{1.3}

\DeclareSourcemap{ \maps[datatype=bibtex]{ \map[overwrite=true]{
      \step[fieldsource=author, match=Deleporte, final]
      \step[fieldset=keywords, fieldvalue=Deleporte] } } }
\begin{document}

\beamertemplatenavigationsymbolsempty

\expandafter\def\expandafter\insertshorttitle\expandafter{%
  \insertshorttitle\hfill%}
}%\insertframenumber}


\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Joint work with Steve Zelditch}
  \begin{center}
    \includegraphics[width=0.8\linewidth]{../img/NemmersSteveCrop.jpg}
    \end{center}
\end{frame}

\begin{frame}
  \frametitle{Kähler geometry}
\myemph{Kähler manifold}: manifold with both complex structure $J$ and
Riemannian metric $g$, with a compatibility condition:
$g(\cdot,J\cdot)$ has to be a symplectic form.

\vfill

Local description (in a holomorphic chart): function $\phi$ (\myemph{Kähler potential}) such
that
\[
  \omega=i\partial \overline{\partial}\phi \qquad \qquad
  g_{jk}=\frac{\partial^2\phi}{\partial z_j\partial \overline{z}_k}.
\]

\vfill

Condition: $\phi$ plurisubharmonic: 
$\frac{\partial^2\phi}{\partial z_j\partial \overline{z}_k}>0$.

\vfill

Examples: \begin{itemize}
\item $\phi=|z|^2$ (standard structure)
  \item $\phi=\log(1+|z|^2)$
    ($2$-sphere in stereographic coordinates).
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Weighted holomorphic spaces}
  Interesting Hilbert spaces on a (piece of) Kähler manifold $\Omega$
  with potential $\phi$:
  \[
    H_k=\left\{u:\Omega\to \C\text{ holomorphic}, \int
      |u|^2e^{-k\phi}\dd\rho_{\phi}<+\infty\right\}.
  \]
  Example if $\Omega=\C$: no $L^2$ holomorphic functions without
  weight but with the $\phi$'s as above, lots of them, and even more
  as $k$ increases!

  \vfill

  \myemph{Global picture}: no global Kähler potential in general, but
  we can glue into \myemph{spaces of holomorphic sections}
  $H^0(M,L^{\otimes k})$.
\end{frame}

\begin{frame}
  \frametitle{Space of Kähler structures}
  Let's vary the Kähler structure! Starting with $(M,J,g_0)$, with
  \[
    \mathcal{H}=\{\phi\in C^{1,1}(M,\R),
    g_0+\partial\overline{\partial}\phi>0\},
  \]
  each $\phi\in \mathcal{H}$ gives a new Kähler structure.


  \vfill

  What happens to $H^0(M,L^{\otimes k})$? It's the same vector space,
  but with a \myemph{new scalar product}.
  \[
    {\rm Hilb}_k:\mathcal{H}\to \mathcal{B}_k:=\text{space of scalar
      products on $H^0(M,L^{\otimes k})$}.
  \]
  There's another natural map
  \[
    {\rm FS}_k:\mathcal{B}_k\to \mathcal{H}
  \]
  given by pulling back the Fubini-Study metric on
  $\C\mathbb{P}^{\dim H^0}$.
\end{frame}

\begin{frame}
  \frametitle{Mabuchi metric}
  \begin{tabular}{|l|c|c|}
    \hline
    &$\mathcal{H}$&$\mathcal{B}_k$\\
    \hline
    tangent space & $C^{1,1}(M,\R)$ & self-adjoint operators\\
    \hline
    sq. norm & $\int |v|^2\dd \rho_{\phi}$ & $\tr(A^2$)\\
    \hline
    geodesics & $\ddot{\phi}=|\nabla \dot{\phi}|^2_{\phi}$ & $<\cdot,  e^{At}\cdot>_{X_0}$\\
    \hline
    curvature & $-\frac 14 \int |\{v,w\}^2|\dd \rho_{\phi}$ & $\frac 14\tr([A,B]^2)$\\
    \hline
  \end{tabular}
  \vfill Differential: Berezin--Toeplitz
  operator
  \[
    {\rm dHilb}_k(\phi,v)=T_k^{\phi}(kv+\Delta_{\phi}v)
  \]
  \uncover<2>{
  \[
    \tr({\rm dHilb}_k(\phi,v)^2)\sim k^{d+2}\int |v|^2\dd \rho_{\phi}
  \]
  \[
    \frac 14\tr([{\rm dHilb}_k(\phi,v)),{\rm dHilb}_k(\phi,v))]^2)\sim
    -\frac 14 k^{d+2}\int |\{v,w\}^2|
  \]}
\end{frame}

\begin{frame}\frametitle{Geodesics and semiclassical limit}
  \begin{itemize}
  \item End-point geodesic problem:
    \begin{itemize}
    \item Existence of solutions in $\mathcal{H}$ \mycite{[Chen 00,
        Blocki 12, Chu-Tosatti-Weinkove 17]}
    \item Convergence of the quantized problem \mycite{[Phong-Sturm
        06, Chen-Sun 12, Finski 22]}
    \end{itemize}
  \item Initial value geodesic problem:
    \begin{itemize}
    \item No existence/uniqueness theorem besides Cauchy-Kowalewskaya
      \mycite{[Mabuchi 87, Rubinstein-Zelditch 17]}
    \item Convergence of the quantized problem in the analytic case:
      \mycite{[D.-Zelditch 22]}
    \end{itemize}
  \end{itemize}

  \vfill

  Main tool: analytic semiclassical analysis of Berezin--Toeplitz
  operators.
\end{frame}



\begin{frame}
  \frametitle{Analytic stationary phase - I}
  Geodesics on $\mathcal{B}_k$= analytic continuations of
  Schrödinger evolutions:

  \[\exp(t\dd {\rm Hilb}_k(\phi,v))=\exp(tkT_k^{\phi}(w))\qquad
    \qquad w=v+k^{-1}\Delta_{\phi}v.\]
  \vfill

  Objective: approximate kernel formulas of WKB type:
  \[
    \exp(tkT_k^{\phi}(w))(x,y)\approx
    k^de^{\Psi(t,x,y)}(a_0(t,x,y)+k^{-1}a_1(t,x,y)+\ldots),
  \]
  known for $t\in i\R$ \mycite{[Chazarain 80, Charles-Le Floch 20]}.

  \vfill
  We need \myemph{exponential accuracy} $O(e^{-ck})$.
\end{frame}

\begin{frame}
  \frametitle{Analytic stationary phase - II}
  
  First step: understand asymptotic properties of integrals
  of the form
  \[
    \int e^{k\varphi(x)}a(x)\dd x.
  \]
  when $\varphi$ and $a$ are real-analytic.

  \vfill
  
  Standard example: $\varphi(x)=-\frac{x^2}{4}$. Usual stationary phase
  \begin{align*}
    \int e^{-\frac{kx^2}{4}}a(x)\dd x&=\frac{1}{\sqrt{\pi k}}\exp(k^{-1} \Delta)a(0)\\
                                     &=\frac{1}{\sqrt{\pi
                                       k}}\sum_{j=0}^N\frac{\Delta^ja(0)}{k^jj!}+O(k^{-N-\frac 32}).
  \end{align*}
  Size of the $j$-th term if $a$ is real-analytic, with convergence
  radius $\rho$:
  \[
    \left|\frac{\Delta^ja(0)}{k^jj!}\right|\leq
    \frac{1}{k^jj!}\|a\|_{C^{2j}}\leq \frac{(2j)!}{\rho^{2j}k^jj!}\leq
    \frac{j!}{(2k\rho^2)^j}.
  \]
\end{frame}
\begin{frame}\frametitle{Analytic stationary phase - III}
  Size of the $j$-th term if $a$ is real-analytic, with convergence
  radius $\rho$:
  \[
    \left|\frac{\Delta^ja(0)}{k^jj!}\right|\leq
    \frac{1}{k^jj!}\|a\|_{C^{2j}}\leq \frac{(2j)!}{\rho^{2j}k^jj!}\leq
    \frac{j!}{(2k\rho^2)^j}.
  \]

  \vfill
        
  The rhs series does not converge!! The smallest term is at
  \[
    j\approx 2k\rho^2,
  \]
  and the size of this term, by Stirling, is:
  \[
    \frac{(2k\rho^2)!}{(2k\rho^2)^{2k\rho^2}}\sim \sqrt{4\pi
      k\rho^2}e^{-2 k \rho^2}.
  \]
\end{frame}

\begin{frame}
  \frametitle{Analytic stationary phase - IIII}
  We obtain analytic stationary phase by {\color{myorange}optimisation
    of the term of the expansion}: for some $\alpha>0,\beta>0$, one
  has
  \[
    \int e^{-\frac{kx^2}{4}}a(x)\dd x=\frac{1}{\sqrt{\pi
        k}}\sum_{j=0}^{\alpha k}\frac{\Delta^ja(0)}{k^jj!}+O(e^{-\beta
      k}).
  \]

  This result can be generalised: the output of the stationary phase
  involves an {\color{myorange}analytic symbol}, up to an
  {\color{myorange}exponentially small} error.
\end{frame}

\begin{frame}
  \frametitle{Analytic symbols}
  \begin{defn}
    A function $a\in \R^n_x\times[0,1]_h\to \C$ is an analytic symbol
    when $a$ is smooth and its Borel transform
    \[
      \mathcal{B}a:(x;h)\mapsto
      \sum_{j=0}^{+\infty}\frac{\partial_h^ja(x;0)h^j}{j!^2}\] sums
    into a real-analytic function near $\{h=0\}$.
  \end{defn}
  In practice, $a=\sum h^ja_j(x)$ is a formal series satisfying
  \[
    \|a_j\|_{C^n}\leq C\frac{j!n!}{\rho^jR^n} \forall j,n.
  \]
  Banach spaces of analytic functions $\rightsquigarrow$ Banach spaces
  of symbols.
\end{frame}

\begin{frame}
  \frametitle{WKB structure of Berezin--Toeplitz operators}
  \begin{theorem}[\mycite{[Deleporte 18, Rouby-Sjöstrand-Vũ Ng\d{o}c 18]}]
    Suppose $\phi,w\in C^{\omega}$. There exists an analytic symbol $a$ and a real-analytic function
    $\Psi$ such that, for some $c>0$,
    \[
      T_k^{\phi}(w)(x,y)=k^de^{k\Psi(x,y)}a(x,y;k^{-1})+O(e^{-ck}).
    \]
  \end{theorem}
  Here $\Psi$ is related to the holomorphic extension of the potential
  $\phi$.

  \begin{theorem}[\mycite{[Deleporte 18]}]
    Given $\phi\in C^{\omega}$ and a space $X_1$ of analytic symbols,
    there exists a larger space $X_2$ of analytic symbols and $c>0$ such that,
    for every $f\in X_1$ and $g\in X_2$, there exists $h\in X_2$ with
    \[
      T_k^{\phi}(f)T_k^{\phi}(g)=T_k^{\phi}(h)+O(e^{-ck})
    \]
    and
    \[
      \|h\|_{X_2}\leq C\|f\|_{X_1}\|g\|_{X_2}.
      \]
  \end{theorem}
\end{frame}

\begin{frame}
  \frametitle{Parametrix for imaginary Schrödinger}
  Let $\phi(t)$ be a Mabuchi geodesic. We know that
  \[
    {\rm Hilb}_k(\phi)(x,y)=k^de^{k\Psi(t)}a(t,x,y;k^{-1})+O(e^{-ck}).
  \]

  \vfill

  \begin{prop}[\mycite{[D.-Zelditch 22]}]For small $t$,
  \[
    e^{tkT_k^{\phi(0)}(\dot{\phi}(0))}(x,y)=k^de^{k\Psi(t)}b(t,x,y;k^{-1})+O(e^{-ck}).
  \]
\end{prop}

  Same phase, different symbol.
\end{frame}

\begin{frame}
  \frametitle{Approximate differential equation}
  By the formal link between Mabuchi and Bergman geodesics (or
 the construction of the Mabuchi geodesics by the method of
 characteristics), we already know that $\Psi(t)$ should be the good
 phase. This means that
 \[
   \frac{\partial}{\partial t}{\rm
     Hilb}_k(\phi)=T_k^{\phi(0)}(k\dot{\phi}(0)+g(t)){\rm
     Hilb}_k(\phi).
 \]
 This means that
 \begin{align*}
   &\frac{\partial}{\partial t}\left[{\rm
     Hilb}_k(\phi)e^{-tkT_k^{\phi(0)}(\dot{\phi}(0))}\right]\\ &=T_k^{\phi(0)}(f(t))\left[{\rm
       Hilb}_k(\phi)e^{-tkT_k^{\phi(0)}(\dot{\phi}(0))}\right]+O(e^{-ck}).
 \end{align*}
\end{frame}

\begin{frame}
  \frametitle{Approximate differential equation - II}
  Given a time-dependent Hamiltonian $f(t)$, we can solve
  \[
    \frac{\partial}{\partial
      t}T_k^{\phi(0)}(b(t))=T_k^{\phi(0)}(f(t))T_k^{\phi(0)}(b(t))+O(e^{-ct})
  \]
  by Picard-Lindelöf! Indeed there exists a Banach space $X_2$ of analytic
  symbols for which multiplication by $T_k^{\phi(0)}(f(t))$ is
  continuous.

  Conclusion: we can write
  \[
    e^{tkT_k^{\phi(0)}(\dot{\phi}(0))}=T_k^{\phi(0)}(b(t)){\rm
      Hilb}_k(\phi(t)),
  \]
  and we're done.
\end{frame}

\begin{frame}
  \frametitle{Perspectives}
  \begin{itemize}
  \item Longer analytic times?
  \item Non-analytic case?
  \item Dequantize into general solutions after explosion time?
  \item Optimal transport?
  \end{itemize}
\end{frame}

\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
