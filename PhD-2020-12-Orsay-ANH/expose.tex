\documentclass[mathserif]{beamer}
\usetheme{Boadilla}
%\usepackage[francais]{babel}
\usepackage[utf8]{inputenc} % Uses the utf8 input encoding
\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs
\usepackage[style=authoryear,backend=biber]{biblatex}
\addbibresource{main.bib}
\usepackage{etoolbox}
\makeatletter
\patchcmd{\@@description}{\advance\beamer@descdefault by
  \labelsep}{\advance\beamer@descdefault by -1em}{}{}
\makeatother

\usepackage{calc}
\usepackage{xcolor}

%\AtBeginSection[]
%{
%\setbeamercolor{section in toc}{fg=alerted text.fg}
%\setbeamercolor{section in toc shaded}{bg=structure!20,fg=structure}
%\setbeamertemplate{section in toc shaded}[default][100]
%\begin{frame}<beamer>
%  \frametitle{Outline}
%  \tableofcontents[currentsection,hideallsubsections]
%\end{frame}
%}

\definecolor{myorange}{RGB}{180,90,0}

\definecolor{mygreen}{RGB}{70,140,0}

\newcommand{\mycite}[1]{{\color{mygreen} \small #1}}

\usepackage[nomath]{kpfonts}
\usepackage{eulervm}
%\usepackage{default}

\input{thm-config}
\title{Szeg\H{o} kernels and Toeplitz operators}
\author{Alix Deleporte}
\date{December 14, 2020}
\institute[LMO]{Laboratoire de Mathématiques d'Orsay\\Université Paris-Saclay}
\newcommand{\spline}{\hline}
\renewcommand{\arraystretch}{1.3}

\DeclareSourcemap{
  \maps[datatype=bibtex]{
    \map[overwrite=true]{
      \step[fieldsource=author,
            match=Deleporte,
            final]
      \step[fieldset=keywords, fieldvalue=Deleporte]
    }
  }
}
\begin{document}

\beamertemplatenavigationsymbolsempty

    \expandafter\def\expandafter\insertshorttitle\expandafter{%
       \insertshorttitle\hfill%}
       }%\insertframenumber}


\begin{frame}
	\titlepage
      \end{frame}

      \begin{frame}
  \frametitle{Positions}
  2016--2019: PhD (Strasbourg)

  \vfill

  2019: Postdoc (MSRI, Berkeley)

  \vfill

  2020: Postdoc (UZH, Zürich)
  
  \vfill
  
  2020--20??: Maître de Conférences (LMO, Paris-Saclay)
\end{frame}

      \begin{frame}
        \frametitle{Outline}
        \begin{enumerate}\setcounter{enumi}{-1}
        \item Motivation: antiferromagnetic spin systems
          \begin{itemize}
          \item What are spin systems?
          \item Frustration in antiferromagnets
          \end{itemize}
        \item Basics of Toeplitz quantization
          \begin{itemize}
          \item What is Toeplitz quantization?
          \item The case of the sphere: spin systems
          \item Semiclassical analysis
          \end{itemize}
        \item Localisation of low-energy states
          \begin{itemize}
          \item Localisation at first order
          \item Localisation at second order
          \item Applications to frustrated spin systems
          \end{itemize}
        \end{enumerate}
      \end{frame}

\begin{frame}
  \frametitle{Introduction}
  \begin{itemize}
  \item<1-> {\bfseries spin systems}: models for magnetism in solids.
  \item<2-> {\bfseries classical spins}: one atom at site $i$
    $\rightsquigarrow$ one spin $s_i\in \S^2$.
  \item<3> Heisenberg antiferromagnet: Graph $G=(V,E)$,
    
    search the minima of the following energy:
    \[
      (s_i)_{i\in V}\mapsto\sum_{i\sim j}s_i\cdot s_j.
    \]
    Here $i\sim j$ when the atoms $i$ and $j$ are neighbours in $G$
    and $\cdot$ is the scalar product.
  \end{itemize}
  \uncover<3>{\begin{center}
    \includegraphics[width=0.2\linewidth]{2spins.png}
  \end{center}}
\end{frame}

             \begin{frame}
         \frametitle{Introduction}
   \begin{center}
     \begin{picture}(50,100)
           \only<1>{\put(-125,-80){\includegraphics[scale=9]{Alcazar.png}}}
           \only<2>{\put(-125,-80){\includegraphics[scale=9]{Alcazar-Kagome.png}}}
           \only<3>{\put(-112,-80){\includegraphics[scale=0.32]{kagome-svg.png}}}
           \only<4>{\put(-112,-80){\includegraphics[scale=0.32]{kagome-spins-svg.png}}}
         \end{picture}
         \vspace{8em}

         Kagome lattice: \uncover<2->{appears in Zn$_2$Cu$_3$(OH)$_6$Cl$_2$ crystals.}
       \end{center}
     \end{frame}
     \begin{frame}
       \frametitle{Introduction}
       On a triangle: $u\cdot v + u \cdot w + v \cdot w=\frac
       12(\|u+v+w\|^2-\underbrace{\|u\|^2-\|v\|^2-\|w\|^2}_{\text{fixed}})$

       \vfill

       \uncover<2>{
         \begin{center}
           \includegraphics[width=0.5\linewidth]{3spins.png}
         \end{center}}
       \end{frame}

       \begin{frame}
         \frametitle{Introduction}
         Minimal set for a loop of 4 triangles :
         \begin{center}
           \includegraphics[width=0.4\linewidth]{3cercles.png}
         \end{center}
         \vfill

         Loop of 6 triangles $\rightsquigarrow$ 3-dim, singular algebraic submanifold of $\mathbb{T}^4$.
       \end{frame}

     \begin{frame}\frametitle{Introduction}
       \begin{itemize}
       \item Quantum spin: self-adjoint matrices
         $S_x,S_y,S_z\in M_{N+1}(\C)$ \[[S_x,S_y]=\frac{2i}{N}S_z\qquad
           [S_y,S_z]=\frac{2i}{N}S_x\qquad [S_z,S_x]=\frac{2i}{N}S_y.\]
       \item Several spins $\rightarrow$ {\color{myorange} tensor product}.
         \[S_{x,i}=\underbrace{Id\otimes \cdots\otimes Id}_{i-1}\otimes S_x\otimes
           \underbrace{Id\otimes
             \cdots
             \otimes
             Id}_{d-i}.\]
         
       \item<2>
         Quantum
         energy
         (matrix
         of
         size
         $(N+1)^d$):
         \[
           \sum_{i\sim j}S_{i,x}S_{j,x}+S_{i,y}S_{j,y}+S_{i,z}S_{j,z}.\]
         
         
      \end{itemize}
    \end{frame}
    \begin{frame}
      \frametitle{Introduction}
      Quantum energy is a self-adjoint matrix $\Rightarrow$
      
      \hspace{1em}eigenvalues
      $\lambda_1\leq \lambda_2\leq \cdots\leq \lambda_{(N+1)^d}$

      \hspace{1em}and associated eigenvectors. \vspace{1em}
      
      Goal: {\color{myorange} qualitative study of eigenvectors} associated with
      $\lambda_1$ (ground states) or other small eigenvalues.
      
      \hfill
      
      \uncover<2>{Physics claim: {\color{myorange} quantum-classical
          correspondence} as $N\to +\infty$.
        
        \hfill
        
      Prediction \mycite{[Douçot-Simon 1998]}: lowest-energy eigenvectors concentrate on {\color{myorange} some} classical configurations.}
    \end{frame}
      \begin{frame}
        \frametitle{Introduction}
        Toeplitz quantization:
        \begin{itemize}
          \item treat $N\to +\infty$ as a semiclassical
       limit
     \item see eigenvectors above as sections over $(\S^2)^d$.
     \end{itemize}
     \uncover<2->{Questions:}
     \begin{itemize}
        \item<2-> Where does the ground state concentrate?
        \item<3> What is the decay speed outside the concentration set?
        \end{itemize}
      \end{frame}

      \begin{frame}
        \frametitle{Introduction}
        \setbeamercovered{transparent}
        {\footnotesize
        \begin{description}
        \item<1>[{[D. 2019]}] Alix Deleporte. “Low-Energy Spectrum of Toeplitz Operators: The Case
of Wells.” In: Journal of Spectral Theory 9 (2019).
\item<1-2> [{[D. 2020a]}] Alix Deleporte. “Low-Energy Spectrum of Toeplitz Operators with a
Miniwell.” In: Comm. Math. Phys 378(3), 1587-1647.
\item<1>[{[D. 2018a++]}] Alix Deleporte. “Quantum Selection for Spin Systems.” In: arXiv
1808.00718 (2018).
\item<1>[{[D. 2018b++]}] Alix Deleporte. “The Bergman Kernel in Constant Curvature.” In:
arXiv 1812.06648 (2018).
\item<1-2>[{[D. 2020b]}] Alix Deleporte. “Toeplitz Operators with Analytic Symbols.” In: Journal of Geometric Analysis, to appear.
\item<1>[{[D. 2019++]}] Alix Deleporte. “WKB Eigenmode Construction for Analytic Toeplitz
  Operators.” In: arXiv 1901.07215 (2019).
  \item<1-2>[{[D. 2020c]}] Alix Deleporte. ``Fractional exponential
    decay in the forbidden region for Toeplitz operators.'' In: Documenta Mathematica (to appear)
\item<1>[{[DHS 2020++]}] Alix Deleporte, Michael Hitrik and Johannes Sjöstrand. “A direct approach to the analytic Bergman projection.” In: arXiv 2004.14606
        \end{description}}
      \end{frame}

      

\section{Basics of Toeplitz quantization}
\begin{frame}
  \frametitle{Semiclassical correspondence}
\begin{center}
	\begin{tabular}{|c|c|}
		\spline
	    Classical mechanics & Quantum mechanics\\
		\spline
		\uncover<2->{Symplectic manifold $M$} & \uncover<2->{Hilbert Space $H$}\\ 
		\spline 
		\uncover<3->{Function $p\in C^{\infty}(M,\R)$}& \uncover<3->{Self-adjoint
                                                   operator $P\in L(H)$}\\
		\spline
          \uncover<4->{Hamiltonien flow $\Phi_t(p)$} &
                                                  \uncover<4->{Unitary
                                                        flow $e^{itP/\hbar}$}\\
		\spline
		\uncover<5->{Poisson Bracket} &\uncover<5->{Lie Bracket}\\
		\spline
	\end{tabular}\end{center}\vspace{0em}
	\uncover<6>{Quantization : associate to $p\in
          C^{\infty}(M,\R)$ a family $Op_{\hbar}(p)\in L(H)$, such that, as
          $\hbar \to 0$,
          \[
            [Op_{\hbar}(p),Op_{\hbar}(q)]=i\hbar Op_{\hbar}(\{p,q\})+O(\hbar^2).
          \]
          
          \vfill
          
          Semiclassical analysis: study $Op_{\hbar}(p)$ as $\hbar\to 0$.

        \vfill

        Example: $M=T^*X$, $H=L^2(X)$, $Op_{\hbar}(\xi)=-i\hbar\nabla$.}
          
      \end{frame}
      
      \begin{frame}
  \frametitle{Bargmann spaces}
  \begin{itemize}
  \item Original idea: express Quantum Mechanics directly in phase
    space. \mycite{[Bargmann 61]}
  \uncover<2->{\item The standard $L^2(\R^n)$ is replaced with the \emph{Bargmann
      space}, with parameter $N>0$:
$$B_N=L^2(\C^n)\cap\left\{e^{-\frac N2 |\cdot|^2}f,\,f\text{ is
  holomorphic on $\C^n$}\right\}.$$}\vspace{-2em}
  \uncover<3>{\item This is a closed subspace of $L^2(\C^n)$, with reproducing
    kernel
$$\Pi_N(x,y)=\left(\frac N{\pi}\right)^n\exp\left(-\cfrac N2
  |x-y|^2+iN\Im(x\cdot \overline{y})\right).$$}
  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Toeplitz quantization}
  Let $f\in C^{\infty}(\C^n,\C)$ bounded. The Toeplitz operator associated
  with $f$ is the bounded operator
\begin{center}
\begin{array}{rcl}
 		T_N(f):B_N(\C^n)&\to & B_N(\C^n)\\
		u& \mapsto& \uncover<2->{\Pi_N(}fu\uncover<2->{)}.
 		\end{array}
\end{center}

\uncover<3->{If $f$ has polynomial growth then $T_N(f)$ is an unbounded operator
with dense domain.}
\uncover<4>{\begin{itemize}
\item If $f$ is real-valued then $T_N(f)$ is ess. self-adjoint.
\item If moreover $f\geq 0$ then $T_N(f)\geq 0$; indeed
  \[
    \langle u,T_N(f)u\rangle=\int f|u|^2.
    \]
\end{itemize}}
\end{frame}

\begin{frame}
  \frametitle{Composition of Toeplitz operators}
  \begin{itemize}
  \item Recipe: \[T_N(z\mapsto \overline{z}^{\alpha}z^{\beta})=N^{-|\alpha|}\partial^{\alpha}z^{\beta}.\]

\uncover<2->{\item The Toeplitz quantization is \emph{anti-Wick}: if $f$ is
  anti-holomorphic and $h$ is holomorphic then
\[T_N(fgh)=T_N(f)T_N(g)T_N(h).\]}
\uncover<3>{\vspace{-1.5em}\item More generally, composition yields a formal series:
\[T_N(f)T_N(g)=T_N\left(fg+N^{-1}C_1(f,g)+N^{-2}C_2(f,g)+\cdots\right).\]

$C_j$ is a bidifferential operator of total order $2j$.}
\end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Toeplitz operators versus $\Psi$DOs}
  \begin{itemize}
  \item The Bargmann transform $\mathcal{B}_N$ conjugates $B_N$ and
    $L^2(\R^n)$.
  \uncover<2->{\item It is related to the FBI
    transform.}
  \uncover<3->{
  \item With $g_N=(N/\pi)^ne^{-N|z|^2}$ one has
    \[
      \mathcal{B}_N^{-1}T_N(f)\mathcal{B}_N=Op_{W}^{N^{-1}}(f*g_N).
    \]\vspace{-2em}}
\uncover<4->{\item Formal equivalence between Toeplitz and $\Psi$DO
  calculus.}
\uncover<5>{\item Toeplitz quantization is formulated {\color{myorange}directly in
  phase space}, and it is {\color{myorange} positive}.}

  \end{itemize}
\end{frame}


\begin{frame}
 \frametitle{Generalized Bargmann spaces}
 Given $\Phi:\R^n\to \R$, let us define
    \[
     B_N^{\Phi}=L^2(\C^n)\cap\left\{e^{-N\Phi}f,\,f\text{ is
  holomorphic on $\C^n$}\right\}
    \]
    \vfill
    
    If $\Phi$ is an arbitrary positive definite quadratic form $\rightarrow$ same theory: 
    the Szeg\H{o} projector is explicit,
    \[
     \exists \Psi,C, \qquad \qquad S_N(z,w)=CN^{-n}\exp(-N\Psi(x,y));
    \]
    and the commutator brings out a new symplectic form

    \[
     [T_N(f),T_N(g)]=\frac{i}{N}T_N(\{f,g\}_\Phi).
    \]
\end{frame}

\begin{frame}
 \frametitle{General definitions}
    Locally: symplectic form $+$ complex structure $\rightsquigarrow$ weight $\Phi$.
    
    \vfill
    
    Gluing together the pieces of Bargmann spaces gives a {\color{myorange} space of holomorphic sections} $H_0(M,L^{\otimes N})$ over the manifold.
    
    \vfill
    
    Szeg\H{o} projector $S_N:L^2(M,L^{\otimes N})\to H_0(M,L^{\otimes N})$, Toeplitz quantization
    \begin{center}
\begin{array}{rcl}
 		T_N(f):H_0(M,L^{\otimes N})&\to & H_0(M,L^{\otimes N})\\
		u& \mapsto& S_N(fu).
 		\end{array}
\end{center}
\end{frame}

\begin{frame}
  \frametitle{Case $M=\S^2$}
  Stereographic chart: $L$ is topologically trivial.

  \hspace{9.5em}Its hermitian metric is
  $\frac{1}{1+|z|^2}|\cdot|^2.$ \[H_0(M,L^{\otimes N})(M)\simeq \left\{f\text{ holo on $
        \C$},\,\int_{\C}\cfrac{|f|^2}{(1+|z|^2)^{N+2}}<\infty\right\}=\C_N[X].\]
 \uncover<2->{Expression of the Szeg\H{o}
 kernel in this chart:\[S_N(z,w)=\cfrac{N+1}{\pi}\left(\cfrac{1+z\overline{w}}{\sqrt{(1+|z|^2)(1+|w|^2)}}\right)^N.\]}
\uncover<3->{
  \only<1-3>{\[T_N(\text{base coords})=\vphantom{\frac{N-2}{N}}\text{spin matrices}.\]}
  \only<4>{\[T_N(\S^2\ni (x,y,z)\mapsto x)=\frac{N-2}{N}S_x.\]}
  }
\end{frame}


\subsection{Semiclassical limit}
\begin{frame}
  \frametitle{The Szeg\H{o} kernel}
 On $\S^2$, there holds $S_N(x,y)=\frac{(N+1)}{\pi}\Psi^{\otimes
      N}(x,y)$, where $\Psi$ is a fixed section over $M\times M$ and
    $\log |\Psi|\asymp -\dist(x,y)^2$.
    \uncover<2->{\begin{theorem}[\mycite{[Delin 1998]}]
        Let $M$ be a compact, {\color{myorange}$C^{1,1}$} Kähler manifold of
        dimension $d$. Then
        \[
          |S_N(x,y)|<CN^d\exp(-c\sqrt{N}\dist(x,y)).
        \]
      \end{theorem}
    }
    \uncover<3->{\begin{theorem}[\mycite{[Charles 2003]}]
        Suppose $M$ is {\color{myorange}smooth}. There exists a section
        $\Psi$ and a sequence of functions $(s_k)_{k\geq 0}$ on
        $M\times M$ such that
      \[
        S_N(x,y)=N^d\Psi^{\otimes
          N}(x,y)(s_0+N^{-1}s_1+\cdots)(x,y)+O(N^{-\infty}).
      \]
      Here, for some $c>0$,
      \[
         |\Psi(x,y)|\leq -c\dist(x,y)^2.
        \]
      \end{theorem}}
    
\end{frame}


\begin{frame}
  \frametitle{Approaches for Szeg\H{o} kernel estimates}
  Off-diagonal decay:
  \begin{itemize}
  \item Spectral gap for $\overline{\partial}^*\overline{\partial}$
    \mycite{[Kohn 1963, Hörmander 1968]}.
  \item Off-diagonal $\exp(-c\sqrt{N})$ decay \mycite{[Christ 1991,
      Delin 1998]}.
  \item Off-diagonal $\exp(-cN)$ decay if $M$ is {\color{myorange} real-analytic}
    \mycite{[Berndtsson-Berman-Sjöstrand 2008]}.
  \end{itemize}

  Near diagonal estimates:
  \begin{itemize}
    \item FIOs with complex phase \mycite{[Shiffman-Zelditch 2002,
        Charles 2003]}: versions of
      the result above, remainder $O(N^{-\infty})$, following
      \mycite{[Boutet de Monvel-Sjöstrand 1975]}.
    \item Weighted estimates on $-\Delta$
      \mycite{[Ma-Marinescu 2007, Ma-Marinsecu-Kordyukov 2019]}:
      generalisations.
    \item Analytic calculus \mycite{[\underline{D. 2020b},
        Rouby-Vũ Ng\d{o}c-Sjöstrand 2019]}: if $M$ is
      {\color{myorange} real-analytic},
      the remainder is $O(e^{-cN})$ with $c>0$.
    \end{itemize}
  \end{frame}

  \begin{frame}
    \frametitle{Application: Calculus of Toeplitz operators}

    \begin{theorem}[{\mycite{[Schlichenmaier 1999]}}]Let $M$ be
      {\color{myorange} smooth}. Let $a$ and $b$ be
      {\color{myorange} smooth} real-valued functions on $M$. Then there exists a sequence $(c_k)_{k\geq
        0}\in (C^{\infty}(M,\R))^{\N}$ such that
      \[
        T_N(a)T_N(b)=T_N(c_0+N^{-1}c_1+\cdots)+O(N^{-\infty}).
        \]

        If $a$ does not vanish, then there exists a sequence
        $(d_k)_{k\geq 0}\in (C^{\infty}(M,\R))^{\N}$ such that
        \[
          T_N(a)T_N(d_0+N^{-1}d_1+\cdots)=S_N+O(N^{-\infty}).
          \]
        \end{theorem}

        \vfill
        
        Calculus {\color{myorange}in analytic regularity} (up to $O(e^{-cN})$):
        \mycite{[\underline{D. 2020b}]}.
  \end{frame}


\section{Localisation of low-energy states}

\subsection{Localisation at the classical minimal set}
\begin{frame}
  \frametitle{Speed of localisation}
 For $N\in \N$, let $u_N$ be a normalised eigenvector associated with
 the smallest eigenvalue of $T_N(f)$. Let $Z=\mathop{argmin}(f)$.
  \begin{theorem}[\mycite{[Charles 2000]}]
  If f is {\color{myorange}smooth} and $U\subset M$ is at positive distance from $Z$, then \[\int_{U}|u_N|^2=O(N^{-\infty}).\]\vspace{-1.2em}
\end{theorem}
\uncover<2>{
\begin{theorem}[\mycite{[\underline{D. 2020b}]}]
If f is {\color{myorange}real-analytic} then\vspace{-1em}
      \[
        \int_{U}|u_N|^2=O(e^{-cN}).%\vspace{-0.5em}
      \]
    \end{theorem}}
  Tool: calculus of Toeplitz operators.
\end{frame}

\begin{frame}
  \frametitle{Localisation in spin systems}
\begin{center}
           \includegraphics[width=0.5\linewidth]{3cercles.png}
         \end{center}
\end{frame}
\begin{frame}
  \frametitle{Speed of localisation -- improved}
  \begin{theorem}[\mycite{[\underline{D. 2020c}]}]
    If $f$ is {\color{myorange} $L^{\infty}$} and $U\subset M$ is at
    positive distance from $Z$, then
    \[
      \int_U |u_N|^2=O(e^{-cN^{\alpha}}).
      \]
    \end{theorem}
    \uncover<2>{
      \begin{itemize}
        \item $M$ is $C^{1,1}$: $\alpha=\frac 14$.
        \item $M$ is analytic: $\alpha=\frac 13$.
        \item $f$ is Lipschitz: $\alpha=\frac 12$, one can take
          $U=\{f\geq \min(f)+CN^{-\frac 12+\epsilon}\}$.
        \item $f$ is $C^2$: $\alpha=\frac 12$, one can take $U=\{f\geq
          \min(f)+CN^{-1+\epsilon}\}$.
        \end{itemize}
      }
      
      \vfill

      Tool: off-diagonal decay of the projector.
    \end{frame}
    
    \begin{frame}
     \frametitle{Uniformity in the dimension}
     
     How do these estimates behave as the dimension of $M$ increases? (In the setting of spin systems: large dimension !)
     
     \begin{theorem}[\mycite{[\underline{D. 2020c}]}]
      Let $f$ be a nice spin system. Then, {\color{myorange}uniformly in $d$ and $N$}, letting
      \[
       U=\left\{x\in (S^2)^d, f(x)\leq \min(f)+C_1N^{-\frac 14}d^{\frac 34}\right\},
      \]
      one has
      \[
       \int |u_N(x)|\exp\left(-c\frac{N^{\frac 12}}{d^{\frac 12}}\dist(x,U)\right)\dd x\leq C_2.
      \]
     \end{theorem}
     Tool: off-diagonal decay of the projector.
    \end{frame}



\subsection{Subprincipal effects on localisation}

\begin{frame}
  \frametitle{Characteristic value}
  \begin{itemize}
  \item For $x\in Z$ a minimal point for $f$, we construct $\mu(x)$
    associated with the Hessian of $f$ at $x$.
  \item $\mu$ captures the eigenvalue contribution of order $N^{-1}$.
  \item<2-> {\color{myorange} Quantum selection}: the ground states of $T_N(f)$
    concentrate only on the subset $Z_{\mu}$ of $Z$ where also $\mu$ is minimal.
  \end{itemize}
  
  \uncover<3>{\begin{itemize}
    \item case of a Schrödinger operator $-\hbar^2\Delta+V$
      \mycite{[Helffer-Sjöstrand 1986]}, where
      \begin{itemize}
      \item $V$ vanishes on a smooth submanifold $Z$
      \item Its transverse Hessian is nondegenerate on $Z$.
      \end{itemize}
    \item case of a magnetic Schrödinger operator with similar
    conditions \mycite{[Helffer-Morame 1996, Helffer-Kordyukov 2009]} .
    \end{itemize}
    }

\end{frame}

\begin{frame}
  \frametitle{Subprincipal localisation}
  Quantum selection in general for Toeplitz operators:
  \begin{theorem}[\mycite{[\underline{D. 2020a}]}]
    Let $M$ be smooth, let $f\in C^{\infty}(M,\R)$ and let $Z_{\mu}$ and
    $(u_N)_{N\geq 1}$ be as above.

    For all $U\subset M$ at positive distance from $Z_{\mu}$, one has
    \[
      \int_U
      |u_N|^2=O(N^{-\infty}).
      \]
  \end{theorem}
\end{frame}

\begin{frame}
  \frametitle{Localisation in spin systems}
\begin{center}
           \includegraphics[width=0.5\linewidth]{3cercles.png}
         \end{center}
\end{frame}

\begin{frame}
  \frametitle{Particular case: frustrated spin systems}
  \begin{center}
    \includegraphics[scale=6]{Alcazar.png}\includegraphics[scale=0.16]{Herbertsmithite.jpg}
  \end{center}
  \begin{itemize}
\item  Framework: fixed number of particles $d$, spin parameter $N$
  tends to infinity, set $Z$ arbitrarily complicated.

  \item<2->{Computations for this particular case
    $M=(\S^2)^d$ \mycite{[\underline{D. 2018b++}]}.}

  \item<3>{{\color{myorange} Open problem}: where is $\mu$ minimal among classical
      configurations on the Kagome lattice?}
  \end{itemize}
\end{frame}

\subsection{Future work}
\begin{frame}
  \frametitle{Perspectives}
    \begin{itemize}
    \item Subprincipal effects for a large number of spins.
    \item Low-energy dynamics.
    \item Non self-adjoint operators with analytic symbols (Scottish flag).
    \end{itemize}
  \end{frame}

  \begin{frame}
    \frametitle{Thanks}
    \centering 
    {\Large Thanks for your attention!}
  \end{frame}

\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
