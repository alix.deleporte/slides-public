\documentclass[mathserif]{beamer}
\usetheme{Boadilla}
%\usepackage[francais]{babel}
\usepackage[utf8]{inputenc} % Uses the utf8 input encoding
\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs
\usepackage{etoolbox}
\makeatletter
\patchcmd{\@@description}{\advance\beamer@descdefault by
  \labelsep}{\advance\beamer@descdefault by -1em}{}{}
\makeatother

\usepackage{calc}
\usepackage{xcolor}

%\AtBeginSection[]
%{
%\setbeamercolor{section in toc}{fg=alerted text.fg}
%\setbeamercolor{section in toc shaded}{bg=structure!20,fg=structure}
%\setbeamertemplate{section in toc shaded}[default][100]
%\begin{frame}<beamer>
%  \frametitle{Outline}
%  \tableofcontents[currentsection,hideallsubsections]
%\end{frame}
%}

\definecolor{myorange}{RGB}{180,90,0}

\definecolor{mygreen}{RGB}{70,140,0}

\newcommand{\mycite}[1]{{\color{mygreen} \small #1}}

\usepackage[nomath]{kpfonts}
\usepackage{eulervm}
%\usepackage{default}

\input{thm-config}
\title{Quantization and Kähler geometry}
\author{Alix Deleporte}
\date{February 5, 2021}
\institute[LMO]{Laboratoire de Mathématiques d'Orsay\\Université Paris-Saclay}
\newcommand{\spline}{\hline}
\renewcommand{\arraystretch}{1.3}

\begin{document}

\beamertemplatenavigationsymbolsempty

    \expandafter\def\expandafter\insertshorttitle\expandafter{%
       \insertshorttitle\hfill%}
       }%\insertframenumber}


\begin{frame}
	\titlepage
      \end{frame}

      \begin{frame}
  \frametitle{Positions}
  2016--2019: PhD (Strasbourg)

  \vfill

  2019: Postdoc (MSRI, Berkeley)

  \vfill

  2020: Postdoc (UZH, Zürich)
  
  \vfill
  
  2020--20??: Maître de Conférences (LMO, Paris-Saclay)
\end{frame}


\begin{frame}
  \frametitle{Quantization}
 \begin{center}
 	\begin{tabular}{|c|c|}
 		\spline
 	    Classical mechanics & Quantum mechanics\\
 		\spline
 		\uncover<2->{Symplectic manifold $M$} & \uncover<2->{Hilbert Space $H$}\\ 
 		\spline 
 		\uncover<3->{Function $p\in C^{\infty}(M,\R)$}& \uncover<3->{Self-adjoint
                                                    operator $P\in L(H)$}\\
 		\spline
           \uncover<4->{Hamiltonien flow $\Phi_t(p)$} &
                                                   \uncover<4->{Unitary
                                                         flow $e^{itP/\hbar}$}\\
 		\spline
 		\uncover<5->{Poisson Bracket} &\uncover<5->{Lie Bracket}\\
 		\spline
 	\end{tabular}\end{center}\vspace{0em}
 	\uncover<6>{Quantization : associate to $p\in
           C^{\infty}(M,\R)$ a family $Op_{\hbar}(p)\in L(H)$, such that, as
           $\hbar \to 0$,
           \[
             [Op_{\hbar}(p),Op_{\hbar}(q)]=i\hbar Op_{\hbar}(\{p,q\})+O(\hbar^2).
           \]
          
           \vfill
          
           Semiclassical analysis: study $Op_{\hbar}(p)$ as $\hbar\to 0$.}

%         Example: $M=T^*X$, $H=L^2(X)$, $Op_{\hbar}(\xi)=-i\hbar\nabla$.} 
       \end{frame}

       
\begin{frame}
  \frametitle{Why you should care about quantization}

  \begin{center}
    \includegraphics[scale=0.3]{../img/Thales.png}
  \end{center}
  Because in 2021, it is ``starting'' to have applications!
\end{frame}

\begin{frame}
  \frametitle{Geometry in PDEs}
  Huygens was among the first to argue that light was a
  {\color{myorange} wave}.

  A monochromatic (wavelength $\lambda << 1$) source at $x_0\in \R^3$
  produces a signal
  \[
    S(y,t)=\frac{1}{\dist(x_0,y)^2}e^{i\omega
      t}{\color{myorange}e^{i\lambda^{-1}\dist(x,y)}}.
  \]
  \uncover<2>{{\color{myorange} Huygens' principle}: every point receiving the
  signal acts as a source itself.

  \[S(z_0,t)=e^{i\omega t}\int
  \frac{1}{\dist(z_0,y)^2}\frac{1}{\dist(y,x_0)^2}{\color{myorange}
    e^{i\lambda^{-1}[\dist(x_0,y)+\dist(y,z_0)]}}\dd y.\]}
\end{frame}

\begin{frame}
  \frametitle{Geometry in PDEs - II}
  {\color{myorange}Stationary phase}: in an integral of the form
  \[
    \int e^{i\lambda^{-1} \phi(y)}a(y)\dd y,
  \]
  if $\lambda$ is large then the contributions from $y$ such that
  $\phi'(y)\neq 0$ are negligible.

  Proof: integration by parts. Primitive of $i\phi'e^{i\lambda^{-1} \phi}$ is
  $\lambda e^{i\lambda^{-1} \phi}$.

 \uncover<2>{ Critical points of
  \[
    y\mapsto \dist(x_0,y)+\dist(y,z_0):
  \]
  {\color{myorange}Light moves in straight
    lines.}

  \vfill

  Connection between wave propagation in $\R^3$
  and classical mechanics on $T^*\R^3$.}
\end{frame}

\begin{frame}
  \frametitle{(pseudo)differential operators}
  $M=(\R^{2d},\omega_{\rm st})$. We separate between
  {\color{myorange} position} variables $x$ and {\color{myorange}
    momentum} variables: $\R^{2d}=\R^d_x\times \R^d_{\xi}$.

  Quantum states are {\color{myorange} functions of the position}
  only: $H=L^2(\R^d_x)$.

  \vfill

  To quantize classical Hamiltonians: replace $\xi$ with
  $-i\hbar\nabla$ (since $[x,-i\hbar \nabla]=i\hbar\times 1$).

  Example: $|\xi|^2$ (moving in straight lines)
  $\rightsquigarrow -\hbar^2\Delta$.

  \vfill

  More precise formula using a double Fourier transform:
  \[
    Op^W_{\hbar}(a):(x,y)\mapsto
    \frac{1}{(2\pi\hbar)^d}\int_{\R^d}e^{i\frac{\langle
        x-y,\xi\rangle}{\hbar}}a\left(\frac{x+y}{2},\xi\right)\dd \xi.
  \]
\end{frame}

\begin{frame}
  \frametitle{$\Psi$DO's from a geometrical perspective}
  What works: linear symplectic transformations yield exact quantum
  transformations. {\color{myorange}Metaplectic representation.}

  \vfill

  What doesn't work: other symplectic transformations even if they
  respect the $\xi$ fibration. In particular, pseudodifferential
  operators on general cotangent spaces $T^*X$ are only defined ``at
  first order'' (modulo $\hbar$, or modulo a differential operator of
  lower degree).

  \vfill

  Other problem: regularity issues. $a$ bounded $\nRightarrow$
  $Op_\hbar^W(a)$ bounded.
\end{frame}

\begin{frame}
  \frametitle{Typical results and questions}
  The dynamical data of the Hamiltonian flow of a function
  $a:T^*M\to \R$ is reflected in the spectrum of $Op^W_{\hbar}(a)$.

  \vfill
    
  Example: closed trajectories with period $T_0$ $\Leftrightarrow$
  subsequence of eigenvalues asymptotically spaced by
  $\frac{2\pi}{T_0}$.

  \vfill
    
  Link between {\color{myorange}spectral rigidity} and
  {\color{myorange}dynamical rigidity} (\mycite{[Kac 66, ...]}) Now we
  understand dynamical zeta functions \mycite{[Faure-Roy-Sjöstrand 08,
    ...]}.

  \vfill

  Hot topic: dynamical ergodicity versus {\color{myorange}quantum
    ergodicity} (eigenfunctions are evenly spread out).
  \mycite{[Anantharaman 07, Dyatlov-... 18-20]}
\end{frame}

\begin{frame}
  \frametitle{Quantization from a Kähler perspective}

  Idea \mycite{[Bargmann 61]}: replace $x,\xi$ coordinates with
  $z,\overline{z}$ coordinates. Quantum states are
  {\color{myorange}holomorphic objects}.

  Bargmann space: $\left\{f\in L^2(\C^d,\C),\,z\mapsto
      e^{\frac{|z|^2}{2\hbar}}f(z)\text{ is entire.}\right\}.$

  \vfill

  \uncover<2>{Generalisation: $(M,J,\omega)$ is a K\"ahler manifold. Complex
  structure $J$ and symplectic structure $\omega$ are compatible:
  locally, $\omega=i\partial\overline{\partial}\phi$, $\phi$ is a
  K\"ahler potential.

  \vfill

  Following Bargmann, quantum states are, locally, holomorphic
  functions, square-integrable with respect to $e^{\frac{\phi}{\hbar}}$.}
\end{frame}
  
\begin{frame}
  \frametitle{Kähler quantization: definitions}
  \begin{itemize}
    \item Let $L\to M$ be a $\C$-bundle with Hermitian structure, such that
  \[{\rm curv}(L)=2i\pi\omega.\]

  \item Quantum space: {\color{myorange} holomorphic sections
    $H_0(M,L^{\otimes k})$}. Here $k=\frac{1}{2\pi\hbar}$.

  \item<2> {\color{myorange}Szeg\H{o} projector}: orthogonal projector $S_k:L^2(M,L^{\otimes
      k})\to H_0(M,L^{\otimes k})$.
  \item<2> Berezin-Toeplitz operator: to $a:M\to \R$ or $\C$, we
    associate
    \[
      T_k(a)=S_kaS_k:H_0(M,L^{\otimes k})\to H_0(M,L^{\otimes k}).\]
  \end{itemize}
  \uncover<2>{This indeed maps the Poisson bracket to the commutator
  \mycite{[Schlichenmaier 00]}.}
\end{frame}
  
\begin{frame}
  \frametitle{Global ``quantized'' conditions}
  Looking at
  \[
    {\rm curv}(L)=2i\pi\omega,
  \]
  we need integer $H^2$ cohomology for $\omega$, i.e.
  \[
    \forall \text{ closed surface } \Sigma\subset M, \int_\Sigma
    2\pi\omega\in \Z.
  \]
  This also means that {\color{myorange}$k$ is quantized} (needs to
  be an integer).
\end{frame}

\begin{frame}
  \frametitle{Asymptotics for the Szeg\H{o} projector}
  \begin{itemize}
    \item On $\C^d$ \mycite{[Bargmann 61]}, $S_k$ has an integral kernel
  \[
    S_k(x,y)=k^d\exp(k(-|x-y|^2+i{\rm Im}(x\cdot
    \overline{y})))=k^d\Psi(x,y)^{\otimes k},
  \]
  where $\Psi$ is the section of $L\boxtimes \overline{L}$,
  holomorphic in $x$, anti-holomorphic in $y$, equal to 1 on the
  diagonal. Remark that
  \[
    |\Psi(x,y)|\approx e^{-\dist(x,y)^2}.
    \]
\item If $M$ is a homogeneous space:
  \[
    S_k(x,y)=\Psi(x,y)^kk^da(x,y,k^{-1}),
  \]
  where $a$ is a degree $d$ polynomial. (Consequence of next slide)

  \vfill
  \item If $M$ is locally homogeneous with constant scalar curvature: true
    up to $O(e^{-ck})$ for $k$ large \mycite{[Deleporte 18]}.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Asymptotics for the Szeg\H{o} projector in general}

  If $M$ is {\color{myorange}$C^{\infty}$} (and/or $J$ is only an almost complex structure) one can define $\Psi$ as an almost
  holomorphic extension of $1$ on the diagonal.
  \begin{theorem}[\mycite{[Tian 98, Zeldtich 98, Charles 03]}]
    As $k\to +\infty$, there is a formal series
    \[
      S_k(x,y)=k^d\Psi^{\otimes
        k}(x,y)(s_0(x,y)+k^{-1}s_1(x,y)+\ldots)+O(k^{-\infty}).
      \]
    \end{theorem}

    If $M$ is {\color{myorange}real-analytic}, $\Psi$ makes sense
    locally.
    \begin{theorem}[\mycite{[Rouby et al. 19, Deleporte 20]}]
      In this case, as $k\to +\infty$, there is an analytic series
      \[
        S_k(x,y)=k^d\Psi^{\otimes
          k}(x,y)(s_0(x,y)+k^{-1}s_1(x,y)+\ldots)+{\color{myorange} O(e^{-ck})}.
        \]
      \end{theorem}
      Application: Spectral theory of Toeplitz operators, peak
      sections, ...
    \end{frame}

\begin{frame}
  \frametitle{Regularity and Toeplitz operators}
  The quantization $T_k(a)$ is well-defined, and bounded, if $a\in
  L^{\infty}(M)$ (contrary to the pseudodifferential case).

  \vfill

  The Szeg\H{o} kernel itself is pointwise bounded if $\omega$ is
  bounded (i.e. Kähler potential $\phi\in C^{1,1}$), and decays away
  from the diagonal like $\exp(-c\sqrt{k}\dist(x,y))$.

  \vfill

  {\color{myorange}Application}: spectrum of operators in a joint
  limit $k\to +\infty$, $d\to +\infty$ \mycite{[Deleporte 20]}
\end{frame}

\begin{frame}
   \frametitle{Towards more general quantum operators}
   Toeplitz operators generalise the previous formula. For every $a$,
   there exists $b$ such that $T_k(a)=S_kaS_k$ has integral kernel
   \[
     T_k(a)(x,y)=k^d\Psi^{k}(x,y)b(x,y,k^{-1}),
   \]
   up to the same level of precision as previously.

   \vfill

   \uncover<2>{Generalisation: operator of the form
   \[
     A_k(x,y)=\Phi^{\otimes k}(x,y)a(x,y,k^{-1}),
   \]
   where $\Phi$ is a section of $L\boxtimes \overline{L}$, and $\Phi$
   and $a$ are holomorphic in $x$, anti-holomorphic in $y$.}
 \end{frame}

 \begin{frame}
   \frametitle{Fourier Integral Operators}
   Important (complexified) object associated with $\Phi$: Lagrangean
   \[
     \widetilde{M}\times \widetilde{M}\supset \mathcal{L}:=\{\nabla_x\widetilde{\Phi}=\nabla_{\overline{y}}\widetilde{\Phi}=0\}.
   \]
   Composition of operators $\leftrightarrow$ composition of
   Lagrangeans
   \[
     (x,z) \ni \mathcal{L}_1\circ \mathcal{L}_2\Leftrightarrow \exists y,
     (x,y)\in \mathcal{L}_1,(y,z)\in \mathcal{L}_2.
   \]

   \uncover<2>{Proof: stationary phase on
   \[
     \int_y [\Psi_1(x,y)\Psi_2(y,z)]^{\otimes
       k}a_1(x,y,k^{-1})a_2(y,z,k^{-1})\dd y:
   \]
   due to the holomorphicity conditions, one needs $\nabla_{\overline{y}}\widetilde{\Psi}_1=\nabla_y\widetilde{\Psi}_2=0$.}
 \end{frame}
 
 \begin{frame}
   \frametitle{Fourier Integral Operators}
   To have well-behaved objects, one needs transversality of
   Lagrangeans with respect to projections on the first, second
   component in $\widetilde{M}\times \widetilde{M}$. At least locally, they generate symplectomorphisms of
   $\widetilde{M}$.

   \vfill

   More algebraic picture: $\mathcal{L} \leftrightarrow$ ideal
   $I_{\phi}$ of
   functions vanishing with $\nabla \Phi$. \mycite{[Hörmander 71]}
 \end{frame}

 \begin{frame}
   \frametitle{Quantization of symplectomorphisms}
   Important example of operators of the form above:
   {\color{myorange}time evolution}. Given $h(t)$ a (smooth/analytic)
   time-dependent Hamiltonian,
   \[
     A_{k}(0)=S_k \qquad \qquad
     ik^{-1}\partial_tA_{k}(t)=T_k(h(t))A_{k}(t)
   \]
   is a Fourier Integral Operator as above, whose Lagrangean is the
   graph of the time flow of $h(t)$.

   In particular, the classical transformation has a quantum
   equivalent.

   \vfill

   \uncover<2>{{\color{myorange}Which symplectomorphisms have quantum
     counterparts?}

   If $\mathcal{L}$ comes from some $\Phi$, then $\Phi$ is determined by
   parallel transport along $\mathcal{L}$.

   Locally, no problem. Globally, again, some {\color{myorange}integer
     cohomology} condition.

   Example: on $\mathbb{T}^2$, the map $(x,y)\mapsto (x+t,y)$ cannot
   be quantized.}
 \end{frame}

 \begin{frame}
   \frametitle{Applications}
{\color{myorange}Bohr atom}: parallel transport along
     classical closed trajectories should ``glue back''.

     \begin{center}
       \includegraphics[scale=0.3]{../img/Bohr-Broglie.png}
     \end{center}
   \end{frame}
   \begin{frame}
     \frametitle{Applications}
{\color{myorange}Little-Parks effect} in
   superconductivity: one can hear the parallel
   transport. \mycite{[Deleporte-V\~u Ng\d{o}c 20]}

   \begin{center}
     \includegraphics[scale=0.4]{../img/LP.jpeg}
   \end{center}
\end{frame}

\begin{frame}
  \frametitle{Applications}

  Quantizing symplectic transforms non-isotopic to the identity:
   {\color{myorange}Quantum catmap}. \mycite{[Zelditch 98]}
\end{frame}

\begin{frame}
  \frametitle{Quantizing changes of symplectic form}
  In the two previous slides: given $(M,J,\omega)$, how to find
  quantum equivalents for maps which preserve $\omega$ but not $J$.

  \vfill

  {\color{myorange}Other way round:} what if we keep $J$ but change
  $\omega$? Space of Kähler metrics on a complex manifold has a
  Riemannian metric (Mabuchi).

  \vfill

  In progress (with S. Zelditch and P. Zhou) : one can quantize the change of
  analytic metric by a FIO. The geodesic equation on the space of metrics
  corresponds to trajectories $t\mapsto \exp(tkT_k(f))$ for $f:M\to
  \R$.
\end{frame}

\begin{frame}
  \frametitle{Perspective}
  \begin{center}
  \begin{tabular}{|c|c|}
    \hline
    Changing $J$ & Changing $\omega$\\
    \hline
    Unitary quantum transforms & Self-adjoint quantum transforms\\
    \hline
    $t\mapsto \exp(itkT_k(f))$& $t\mapsto \exp(tkT_k(f))$\\
    \hline
    Parallel transport condition & new $\omega$ in the same class\\
    \hline
    $L^2$ displacement energy & Mabuchi metric\\
    \hline
    ??? & Ricci flow\\
    \hline
  \end{tabular}
\end{center}

We hope to understand this picture at least in real-analytic regularity.
\end{frame}
 
%\begin{frame}
%         \frametitle{Outline}
%         \begin{enumerate}\setcounter{enumi}{-1}
%         \item Motivation: antiferromagnetic spin systems
%           \begin{itemize}
%           \item What are spin systems?
%           \item Frustration in antiferromagnets
%           \end{itemize}
%         \item Basics of Toeplitz quantization
%           \begin{itemize}
%           \item What is Toeplitz quantization?
%           \item The case of the sphere: spin systems
%           \item Semiclassical analysis
%           \end{itemize}
%         \item Localisation of low-energy states
%           \begin{itemize}
%           \item Localisation at first order
%           \item Localisation at second order
%           \item Applications to frustrated spin systems
%           \end{itemize}
%         \end{enumerate}
%       \end{frame}

% \begin{frame}
%   \frametitle{Introduction}
%   \begin{itemize}
%   \item<1-> {\bfseries spin systems}: models for magnetism in solids.
%   \item<2-> {\bfseries classical spins}: one atom at site $i$
%     $\rightsquigarrow$ one spin $s_i\in \S^2$.
%   \item<3> Heisenberg antiferromagnet: Graph $G=(V,E)$,
    
%     search the minima of the following energy:
%     \[
%       (s_i)_{i\in V}\mapsto\sum_{i\sim j}s_i\cdot s_j.
%     \]
%     Here $i\sim j$ when the atoms $i$ and $j$ are neighbours in $G$
%     and $\cdot$ is the scalar product.
%   \end{itemize}
%   \uncover<3>{\begin{center}
%     \includegraphics[width=0.2\linewidth]{2spins.png}
%   \end{center}}
% \end{frame}

%              \begin{frame}
%          \frametitle{Introduction}
%    \begin{center}
%      \begin{picture}(50,100)
%            \only<1>{\put(-125,-80){\includegraphics[scale=9]{Alcazar.png}}}
%            \only<2>{\put(-125,-80){\includegraphics[scale=9]{Alcazar-Kagome.png}}}
%            \only<3>{\put(-112,-80){\includegraphics[scale=0.32]{kagome-svg.png}}}
%            \only<4>{\put(-112,-80){\includegraphics[scale=0.32]{kagome-spins-svg.png}}}
%          \end{picture}
%          \vspace{8em}

%          Kagome lattice: \uncover<2->{appears in Zn$_2$Cu$_3$(OH)$_6$Cl$_2$ crystals.}
%        \end{center}
%      \end{frame}
%      \begin{frame}
%        \frametitle{Introduction}
%        On a triangle: $u\cdot v + u \cdot w + v \cdot w=\frac
%        12(\|u+v+w\|^2-\underbrace{\|u\|^2-\|v\|^2-\|w\|^2}_{\text{fixed}})$

%        \vfill

%        \uncover<2>{
%          \begin{center}
%            \includegraphics[width=0.5\linewidth]{3spins.png}
%          \end{center}}
%        \end{frame}

%        \begin{frame}
%          \frametitle{Introduction}
%          Minimal set for a loop of 4 triangles :
%          \begin{center}
%            \includegraphics[width=0.4\linewidth]{3cercles.png}
%          \end{center}
%          \vfill

%          Loop of 6 triangles $\rightsquigarrow$ 3-dim, singular algebraic submanifold of $\mathbb{T}^4$.
%        \end{frame}

%      \begin{frame}\frametitle{Introduction}
%        \begin{itemize}
%        \item Quantum spin: self-adjoint matrices
%          $S_x,S_y,S_z\in M_{N+1}(\C)$ \[[S_x,S_y]=\frac{2i}{N}S_z\qquad
%            [S_y,S_z]=\frac{2i}{N}S_x\qquad [S_z,S_x]=\frac{2i}{N}S_y.\]
%        \item Several spins $\rightarrow$ {\color{myorange} tensor product}.
%          \[S_{x,i}=\underbrace{Id\otimes \cdots\otimes Id}_{i-1}\otimes S_x\otimes
%            \underbrace{Id\otimes
%              \cdots
%              \otimes
%              Id}_{d-i}.\]
         
%        \item<2>
%          Quantum
%          energy
%          (matrix
%          of
%          size
%          $(N+1)^d$):
%          \[
%            \sum_{i\sim j}S_{i,x}S_{j,x}+S_{i,y}S_{j,y}+S_{i,z}S_{j,z}.\]
         
         
%       \end{itemize}
%     \end{frame}
%     \begin{frame}
%       \frametitle{Introduction}
%       Quantum energy is a self-adjoint matrix $\Rightarrow$
      
%       \hspace{1em}eigenvalues
%       $\lambda_1\leq \lambda_2\leq \cdots\leq \lambda_{(N+1)^d}$

%       \hspace{1em}and associated eigenvectors. \vspace{1em}
      
%       Goal: {\color{myorange} qualitative study of eigenvectors} associated with
%       $\lambda_1$ (ground states) or other small eigenvalues.
      
%       \hfill
      
%       \uncover<2>{Physics claim: {\color{myorange} quantum-classical
%           correspondence} as $N\to +\infty$.
        
%         \hfill
        
%       Prediction \mycite{[Douçot-Simon 1998]}: lowest-energy eigenvectors concentrate on {\color{myorange} some} classical configurations.}
%     \end{frame}
%       \begin{frame}
%         \frametitle{Introduction}
%         Toeplitz quantization:
%         \begin{itemize}
%           \item treat $N\to +\infty$ as a semiclassical
%        limit
%      \item see eigenvectors above as sections over $(\S^2)^d$.
%      \end{itemize}
%      \uncover<2->{Questions:}
%      \begin{itemize}
%         \item<2-> Where does the ground state concentrate?
%         \item<3> What is the decay speed outside the concentration set?
%         \end{itemize}
%       \end{frame}

%       \begin{frame}
%         \frametitle{Introduction}
%         \setbeamercovered{transparent}
%         {\footnotesize
%         \begin{description}
%         \item<1>[{[D. 2019]}] Alix Deleporte. “Low-Energy Spectrum of Toeplitz Operators: The Case
% of Wells.” In: Journal of Spectral Theory 9 (2019).
% \item<1-2> [{[D. 2020a]}] Alix Deleporte. “Low-Energy Spectrum of Toeplitz Operators with a
% Miniwell.” In: Comm. Math. Phys 378(3), 1587-1647.
% \item<1>[{[D. 2018a++]}] Alix Deleporte. “Quantum Selection for Spin Systems.” In: arXiv
% 1808.00718 (2018).
% \item<1>[{[D. 2018b++]}] Alix Deleporte. “The Bergman Kernel in Constant Curvature.” In:
% arXiv 1812.06648 (2018).
% \item<1-2>[{[D. 2020b]}] Alix Deleporte. “Toeplitz Operators with Analytic Symbols.” In: Journal of Geometric Analysis, to appear.
% \item<1>[{[D. 2019++]}] Alix Deleporte. “WKB Eigenmode Construction for Analytic Toeplitz
%   Operators.” In: arXiv 1901.07215 (2019).
%   \item<1-2>[{[D. 2020c]}] Alix Deleporte. ``Fractional exponential
%     decay in the forbidden region for Toeplitz operators.'' In: Documenta Mathematica (to appear)
% \item<1>[{[DHS 2020++]}] Alix Deleporte, Michael Hitrik and Johannes Sjöstrand. “A direct approach to the analytic Bergman projection.” In: arXiv 2004.14606
%         \end{description}}
%       \end{frame}

      

% \section{Basics of Toeplitz quantization}
% \begin{frame}
%   \frametitle{Semiclassical correspondence}
% \begin{center}
% 	\begin{tabular}{|c|c|}
% 		\spline
% 	    Classical mechanics & Quantum mechanics\\
% 		\spline
% 		\uncover<2->{Symplectic manifold $M$} & \uncover<2->{Hilbert Space $H$}\\ 
% 		\spline 
% 		\uncover<3->{Function $p\in C^{\infty}(M,\R)$}& \uncover<3->{Self-adjoint
%                                                    operator $P\in L(H)$}\\
% 		\spline
%           \uncover<4->{Hamiltonien flow $\Phi_t(p)$} &
%                                                   \uncover<4->{Unitary
%                                                         flow $e^{itP/\hbar}$}\\
% 		\spline
% 		\uncover<5->{Poisson Bracket} &\uncover<5->{Lie Bracket}\\
% 		\spline
% 	\end{tabular}\end{center}\vspace{0em}
% 	\uncover<6>{Quantization : associate to $p\in
%           C^{\infty}(M,\R)$ a family $Op_{\hbar}(p)\in L(H)$, such that, as
%           $\hbar \to 0$,
%           \[
%             [Op_{\hbar}(p),Op_{\hbar}(q)]=i\hbar Op_{\hbar}(\{p,q\})+O(\hbar^2).
%           \]
          
%           \vfill
          
%           Semiclassical analysis: study $Op_{\hbar}(p)$ as $\hbar\to 0$.

%         \vfill

%         Example: $M=T^*X$, $H=L^2(X)$, $Op_{\hbar}(\xi)=-i\hbar\nabla$.}
          
%       \end{frame}
      
%       \begin{frame}
%   \frametitle{Bargmann spaces}
%   \begin{itemize}
%   \item Original idea: express Quantum Mechanics directly in phase
%     space. \mycite{[Bargmann 61]}
%   \uncover<2->{\item The standard $L^2(\R^n)$ is replaced with the \emph{Bargmann
%       space}, with parameter $N>0$:
% $$B_N=L^2(\C^n)\cap\left\{e^{-\frac N2 |\cdot|^2}f,\,f\text{ is
%   holomorphic on $\C^n$}\right\}.$$}\vspace{-2em}
%   \uncover<3>{\item This is a closed subspace of $L^2(\C^n)$, with reproducing
%     kernel
% $$\Pi_N(x,y)=\left(\frac N{\pi}\right)^n\exp\left(-\cfrac N2
%   |x-y|^2+iN\Im(x\cdot \overline{y})\right).$$}
%   \end{itemize}

% \end{frame}

% \begin{frame}
%   \frametitle{Toeplitz quantization}
%   Let $f\in C^{\infty}(\C^n,\C)$ bounded. The Toeplitz operator associated
%   with $f$ is the bounded operator
% \begin{center}
% \begin{array}{rcl}
%  		T_N(f):B_N(\C^n)&\to & B_N(\C^n)\\
% 		u& \mapsto& \uncover<2->{\Pi_N(}fu\uncover<2->{)}.
%  		\end{array}
% \end{center}

% \uncover<3->{If $f$ has polynomial growth then $T_N(f)$ is an unbounded operator
% with dense domain.}
% \uncover<4>{\begin{itemize}
% \item If $f$ is real-valued then $T_N(f)$ is ess. self-adjoint.
% \item If moreover $f\geq 0$ then $T_N(f)\geq 0$; indeed
%   \[
%     \langle u,T_N(f)u\rangle=\int f|u|^2.
%     \]
% \end{itemize}}
% \end{frame}

% \begin{frame}
%   \frametitle{Composition of Toeplitz operators}
%   \begin{itemize}
%   \item Recipe: \[T_N(z\mapsto \overline{z}^{\alpha}z^{\beta})=N^{-|\alpha|}\partial^{\alpha}z^{\beta}.\]

% \uncover<2->{\item The Toeplitz quantization is \emph{anti-Wick}: if $f$ is
%   anti-holomorphic and $h$ is holomorphic then
% \[T_N(fgh)=T_N(f)T_N(g)T_N(h).\]}
% \uncover<3>{\vspace{-1.5em}\item More generally, composition yields a formal series:
% \[T_N(f)T_N(g)=T_N\left(fg+N^{-1}C_1(f,g)+N^{-2}C_2(f,g)+\cdots\right).\]

% $C_j$ is a bidifferential operator of total order $2j$.}
% \end{itemize}
% \end{frame}

% \begin{frame}
%   \frametitle{Toeplitz operators versus $\Psi$DOs}
%   \begin{itemize}
%   \item The Bargmann transform $\mathcal{B}_N$ conjugates $B_N$ and
%     $L^2(\R^n)$.
%   \uncover<2->{\item It is related to the FBI
%     transform.}
%   \uncover<3->{
%   \item With $g_N=(N/\pi)^ne^{-N|z|^2}$ one has
%     \[
%       \mathcal{B}_N^{-1}T_N(f)\mathcal{B}_N=Op_{W}^{N^{-1}}(f*g_N).
%     \]\vspace{-2em}}
% \uncover<4->{\item Formal equivalence between Toeplitz and $\Psi$DO
%   calculus.}
% \uncover<5>{\item Toeplitz quantization is formulated {\color{myorange}directly in
%   phase space}, and it is {\color{myorange} positive}.}

%   \end{itemize}
% \end{frame}


% \begin{frame}
%  \frametitle{Generalized Bargmann spaces}
%  Given $\Phi:\R^n\to \R$, let us define
%     \[
%      B_N^{\Phi}=L^2(\C^n)\cap\left\{e^{-N\Phi}f,\,f\text{ is
%   holomorphic on $\C^n$}\right\}
%     \]
%     \vfill
    
%     If $\Phi$ is an arbitrary positive definite quadratic form $\rightarrow$ same theory: 
%     the Szeg\H{o} projector is explicit,
%     \[
%      \exists \Psi,C, \qquad \qquad S_N(z,w)=CN^{-n}\exp(-N\Psi(x,y));
%     \]
%     and the commutator brings out a new symplectic form

%     \[
%      [T_N(f),T_N(g)]=\frac{i}{N}T_N(\{f,g\}_\Phi).
%     \]
% \end{frame}

% \begin{frame}
%  \frametitle{General definitions}
%     Locally: symplectic form $+$ complex structure $\rightsquigarrow$ weight $\Phi$.
    
%     \vfill
    
%     Gluing together the pieces of Bargmann spaces gives a {\color{myorange} space of holomorphic sections} $H_0(M,L^{\otimes N})$ over the manifold.
    
%     \vfill
    
%     Szeg\H{o} projector $S_N:L^2(M,L^{\otimes N})\to H_0(M,L^{\otimes N})$, Toeplitz quantization
%     \begin{center}
% \begin{array}{rcl}
%  		T_N(f):H_0(M,L^{\otimes N})&\to & H_0(M,L^{\otimes N})\\
% 		u& \mapsto& S_N(fu).
%  		\end{array}
% \end{center}
% \end{frame}

% \begin{frame}
%   \frametitle{Case $M=\S^2$}
%   Stereographic chart: $L$ is topologically trivial.

%   \hspace{9.5em}Its hermitian metric is
%   $\frac{1}{1+|z|^2}|\cdot|^2.$ \[H_0(M,L^{\otimes N})(M)\simeq \left\{f\text{ holo on $
%         \C$},\,\int_{\C}\cfrac{|f|^2}{(1+|z|^2)^{N+2}}<\infty\right\}=\C_N[X].\]
%  \uncover<2->{Expression of the Szeg\H{o}
%  kernel in this chart:\[S_N(z,w)=\cfrac{N+1}{\pi}\left(\cfrac{1+z\overline{w}}{\sqrt{(1+|z|^2)(1+|w|^2)}}\right)^N.\]}
% \uncover<3->{
%   \only<1-3>{\[T_N(\text{base coords})=\vphantom{\frac{N-2}{N}}\text{spin matrices}.\]}
%   \only<4>{\[T_N(\S^2\ni (x,y,z)\mapsto x)=\frac{N-2}{N}S_x.\]}
%   }
% \end{frame}


% \subsection{Semiclassical limit}
% \begin{frame}
%   \frametitle{The Szeg\H{o} kernel}
%  On $\S^2$, there holds $S_N(x,y)=\frac{(N+1)}{\pi}\Psi^{\otimes
%       N}(x,y)$, where $\Psi$ is a fixed section over $M\times M$ and
%     $\log |\Psi|\asymp -\dist(x,y)^2$.
%     \uncover<2->{\begin{theorem}[\mycite{[Delin 1998]}]
%         Let $M$ be a compact, {\color{myorange}$C^{1,1}$} Kähler manifold of
%         dimension $d$. Then
%         \[
%           |S_N(x,y)|<CN^d\exp(-c\sqrt{N}\dist(x,y)).
%         \]
%       \end{theorem}
%     }
%     \uncover<3->{\begin{theorem}[\mycite{[Charles 2003]}]
%         Suppose $M$ is {\color{myorange}smooth}. There exists a section
%         $\Psi$ and a sequence of functions $(s_k)_{k\geq 0}$ on
%         $M\times M$ such that
%       \[
%         S_N(x,y)=N^d\Psi^{\otimes
%           N}(x,y)(s_0+N^{-1}s_1+\cdots)(x,y)+O(N^{-\infty}).
%       \]
%       Here, for some $c>0$,
%       \[
%          |\Psi(x,y)|\leq -c\dist(x,y)^2.
%         \]
%       \end{theorem}}
    
% \end{frame}


% \begin{frame}
%   \frametitle{Approaches for Szeg\H{o} kernel estimates}
%   Off-diagonal decay:
%   \begin{itemize}
%   \item Spectral gap for $\overline{\partial}^*\overline{\partial}$
%     \mycite{[Kohn 1963, Hörmander 1968]}.
%   \item Off-diagonal $\exp(-c\sqrt{N})$ decay \mycite{[Christ 1991,
%       Delin 1998]}.
%   \item Off-diagonal $\exp(-cN)$ decay if $M$ is {\color{myorange} real-analytic}
%     \mycite{[Berndtsson-Berman-Sjöstrand 2008]}.
%   \end{itemize}

%   Near diagonal estimates:
%   \begin{itemize}
%     \item FIOs with complex phase \mycite{[Shiffman-Zelditch 2002,
%         Charles 2003]}: versions of
%       the result above, remainder $O(N^{-\infty})$, following
%       \mycite{[Boutet de Monvel-Sjöstrand 1975]}.
%     \item Weighted estimates on $-\Delta$
%       \mycite{[Ma-Marinescu 2007, Ma-Marinsecu-Kordyukov 2019]}:
%       generalisations.
%     \item Analytic calculus \mycite{[\underline{D. 2020b},
%         Rouby-Vũ Ng\d{o}c-Sjöstrand 2019]}: if $M$ is
%       {\color{myorange} real-analytic},
%       the remainder is $O(e^{-cN})$ with $c>0$.
%     \end{itemize}
%   \end{frame}

%   \begin{frame}
%     \frametitle{Application: Calculus of Toeplitz operators}

%     \begin{theorem}[{\mycite{[Schlichenmaier 1999]}}]Let $M$ be
%       {\color{myorange} smooth}. Let $a$ and $b$ be
%       {\color{myorange} smooth} real-valued functions on $M$. Then there exists a sequence $(c_k)_{k\geq
%         0}\in (C^{\infty}(M,\R))^{\N}$ such that
%       \[
%         T_N(a)T_N(b)=T_N(c_0+N^{-1}c_1+\cdots)+O(N^{-\infty}).
%         \]

%         If $a$ does not vanish, then there exists a sequence
%         $(d_k)_{k\geq 0}\in (C^{\infty}(M,\R))^{\N}$ such that
%         \[
%           T_N(a)T_N(d_0+N^{-1}d_1+\cdots)=S_N+O(N^{-\infty}).
%           \]
%         \end{theorem}

%         \vfill
        
%         Calculus {\color{myorange}in analytic regularity} (up to $O(e^{-cN})$):
%         \mycite{[\underline{D. 2020b}]}.
%   \end{frame}


% \section{Localisation of low-energy states}

% \subsection{Localisation at the classical minimal set}
% \begin{frame}
%   \frametitle{Speed of localisation}
%  For $N\in \N$, let $u_N$ be a normalised eigenvector associated with
%  the smallest eigenvalue of $T_N(f)$. Let $Z=\mathop{argmin}(f)$.
%   \begin{theorem}[\mycite{[Charles 2000]}]
%   If f is {\color{myorange}smooth} and $U\subset M$ is at positive distance from $Z$, then \[\int_{U}|u_N|^2=O(N^{-\infty}).\]\vspace{-1.2em}
% \end{theorem}
% \uncover<2>{
% \begin{theorem}[\mycite{[\underline{D. 2020b}]}]
% If f is {\color{myorange}real-analytic} then\vspace{-1em}
%       \[
%         \int_{U}|u_N|^2=O(e^{-cN}).%\vspace{-0.5em}
%       \]
%     \end{theorem}}
%   Tool: calculus of Toeplitz operators.
% \end{frame}

% \begin{frame}
%   \frametitle{Localisation in spin systems}
% \begin{center}
%            \includegraphics[width=0.5\linewidth]{3cercles.png}
%          \end{center}
% \end{frame}
% \begin{frame}
%   \frametitle{Speed of localisation -- improved}
%   \begin{theorem}[\mycite{[\underline{D. 2020c}]}]
%     If $f$ is {\color{myorange} $L^{\infty}$} and $U\subset M$ is at
%     positive distance from $Z$, then
%     \[
%       \int_U |u_N|^2=O(e^{-cN^{\alpha}}).
%       \]
%     \end{theorem}
%     \uncover<2>{
%       \begin{itemize}
%         \item $M$ is $C^{1,1}$: $\alpha=\frac 14$.
%         \item $M$ is analytic: $\alpha=\frac 13$.
%         \item $f$ is Lipschitz: $\alpha=\frac 12$, one can take
%           $U=\{f\geq \min(f)+CN^{-\frac 12+\epsilon}\}$.
%         \item $f$ is $C^2$: $\alpha=\frac 12$, one can take $U=\{f\geq
%           \min(f)+CN^{-1+\epsilon}\}$.
%         \end{itemize}
%       }
      
%       \vfill

%       Tool: off-diagonal decay of the projector.
%     \end{frame}
    
%     \begin{frame}
%      \frametitle{Uniformity in the dimension}
     
%      How do these estimates behave as the dimension of $M$ increases? (In the setting of spin systems: large dimension !)
     
%      \begin{theorem}[\mycite{[\underline{D. 2020c}]}]
%       Let $f$ be a nice spin system. Then, {\color{myorange}uniformly in $d$ and $N$}, letting
%       \[
%        U=\left\{x\in (S^2)^d, f(x)\leq \min(f)+C_1N^{-\frac 14}d^{\frac 34}\right\},
%       \]
%       one has
%       \[
%        \int |u_N(x)|\exp\left(-c\frac{N^{\frac 12}}{d^{\frac 12}}\dist(x,U)\right)\dd x\leq C_2.
%       \]
%      \end{theorem}
%      Tool: off-diagonal decay of the projector.
%     \end{frame}



% \subsection{Subprincipal effects on localisation}

% \begin{frame}
%   \frametitle{Characteristic value}
%   \begin{itemize}
%   \item For $x\in Z$ a minimal point for $f$, we construct $\mu(x)$
%     associated with the Hessian of $f$ at $x$.
%   \item $\mu$ captures the eigenvalue contribution of order $N^{-1}$.
%   \item<2-> {\color{myorange} Quantum selection}: the ground states of $T_N(f)$
%     concentrate only on the subset $Z_{\mu}$ of $Z$ where also $\mu$ is minimal.
%   \end{itemize}
  
%   \uncover<3>{\begin{itemize}
%     \item case of a Schrödinger operator $-\hbar^2\Delta+V$
%       \mycite{[Helffer-Sjöstrand 1986]}, where
%       \begin{itemize}
%       \item $V$ vanishes on a smooth submanifold $Z$
%       \item Its transverse Hessian is nondegenerate on $Z$.
%       \end{itemize}
%     \item case of a magnetic Schrödinger operator with similar
%     conditions \mycite{[Helffer-Morame 1996, Helffer-Kordyukov 2009]} .
%     \end{itemize}
%     }

% \end{frame}

% \begin{frame}
%   \frametitle{Subprincipal localisation}
%   Quantum selection in general for Toeplitz operators:
%   \begin{theorem}[\mycite{[\underline{D. 2020a}]}]
%     Let $M$ be smooth, let $f\in C^{\infty}(M,\R)$ and let $Z_{\mu}$ and
%     $(u_N)_{N\geq 1}$ be as above.

%     For all $U\subset M$ at positive distance from $Z_{\mu}$, one has
%     \[
%       \int_U
%       |u_N|^2=O(N^{-\infty}).
%       \]
%   \end{theorem}
% \end{frame}

% \begin{frame}
%   \frametitle{Localisation in spin systems}
% \begin{center}
%            \includegraphics[width=0.5\linewidth]{3cercles.png}
%          \end{center}
% \end{frame}

% \begin{frame}
%   \frametitle{Particular case: frustrated spin systems}
%   \begin{center}
%     \includegraphics[scale=6]{Alcazar.png}\includegraphics[scale=0.16]{Herbertsmithite.jpg}
%   \end{center}
%   \begin{itemize}
% \item  Framework: fixed number of particles $d$, spin parameter $N$
%   tends to infinity, set $Z$ arbitrarily complicated.

%   \item<2->{Computations for this particular case
%     $M=(\S^2)^d$ \mycite{[\underline{D. 2018b++}]}.}

%   \item<3>{{\color{myorange} Open problem}: where is $\mu$ minimal among classical
%       configurations on the Kagome lattice?}
%   \end{itemize}
% \end{frame}

% \subsection{Future work}
% \begin{frame}
%   \frametitle{Perspectives}
%     \begin{itemize}
%     \item Subprincipal effects for a large number of spins.
%     \item Low-energy dynamics.
%     \item Non self-adjoint operators with analytic symbols (Scottish flag).
%     \end{itemize}
%   \end{frame}

   \begin{frame}
     \frametitle{Thanks}
     \centering 
     {\Large Thanks for your attention!}
   \end{frame}

\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
