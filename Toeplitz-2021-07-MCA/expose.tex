\documentclass[mathserif]{beamer}
\usetheme{Boadilla}
%\usepackage[francais]{babel}
\usepackage[utf8]{inputenc} % Uses the utf8 input encoding
\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs
\usepackage[style=authoryear,backend=biber]{biblatex}
\addbibresource{main.bib}
\usepackage{etoolbox}
\makeatletter
\patchcmd{\@@description}{\advance\beamer@descdefault by
  \labelsep}{\advance\beamer@descdefault by -1em}{}{}
\makeatother

\usepackage{calc}
\usepackage{xcolor}

%\AtBeginSection[]
%{
%\setbeamercolor{section in toc}{fg=alerted text.fg}
%\setbeamercolor{section in toc shaded}{bg=structure!20,fg=structure}
%\setbeamertemplate{section in toc shaded}[default][100]
%\begin{frame}<beamer>
%  \frametitle{Outline}
%  \tableofcontents[currentsection,hideallsubsections]
%\end{frame}
%}

\definecolor{myorange}{RGB}{180,90,0}

\definecolor{mygreen}{RGB}{70,140,0}

\newcommand{\mycite}[1]{{\color{mygreen} \small #1}}

\usepackage[nomath]{kpfonts}
\usepackage{eulervm}
%\usepackage{default}

\input{thm-config}
\title[Toeplitz quantization]{Introduction to Toeplitz quantization}
\author{Alix Deleporte}
\date[MCA 2021]{Mathematical Congress of the Americas 2021}
\institute[LMO]{Laboratoire de Mathématiques d'Orsay\\Université Paris-Saclay}
\newcommand{\spline}{\hline}
\renewcommand{\arraystretch}{1.3}

\DeclareSourcemap{
  \maps[datatype=bibtex]{
    \map[overwrite=true]{
      \step[fieldsource=author,
            match=Deleporte,
            final]
      \step[fieldset=keywords, fieldvalue=Deleporte]
    }
  }
}
\begin{document}

\beamertemplatenavigationsymbolsempty

    \expandafter\def\expandafter\insertshorttitle\expandafter{%
       \insertshorttitle\hfill%}
       }%\insertframenumber}


\begin{frame}
	\titlepage
      \end{frame}

\begin{frame}
  \frametitle{Quantization of the harmonic oscillator}
  Quantization: associate {\color{myorange} classical dynamics} (driven by
  real-valued functions) with {\color{myorange} self-adjoint operators}.

  \uncover<1->{\begin{center}
    \begin{tabular}{ccc}
      classical & $\rightsquigarrow$ & quantum\\\hline
      \uncover<2->{\includegraphics[scale=0.2]{../img/harm_phase.png}}&&\only<5>{\put(-10,45){???}}\\
      \uncover<3->{\tiny $\ddot{x}(t)=-x(t)$}& & \uncover<4->{\tiny
                                                 $i\hbar \partial_t\psi=(-\hbar^2\Delta+x^2)\psi$}\\
      \uncover<3->{{\tiny
      $x(t)=\sqrt{x_0^2+\xi_0^2}\cos\left[t-\arctan\left(\frac{\xi_0}{x_0}\right)\right]$}}&
                                     &\uncover<4->{{\tiny $H_{n;\hbar}(x)e^{-\frac{x^2}{2\hbar}}$}}
    \end{tabular}
  \end{center}}
\end{frame}

\begin{frame}
  \frametitle{Bargmann spaces}
  \begin{itemize}
  \uncover<1->{\item Replace $L^2(\R^n)$ with the \emph{Bargmann
      space}, with parameter $N>0$ (think
    $N=\hbar^{-1}$). \mycite{[Bargmann 61]}
$$B_N=L^2(\C^n)\cap\left\{e^{-\frac N2 |\cdot|^2}f,\,f\text{ is
  holomorphic on $\C^n$}\right\}.$$}\vspace{-2em}
  \uncover<2->{\item This is a closed subspace of $L^2(\C^n)$, with reproducing
    kernel
$$\Pi_N(x,y)=\left(\frac N{\pi}\right)^n\exp\left(-\cfrac N2
  |x-y|^2+iN\Im(x\cdot \overline{y})\right).$$
\item Interpretation: eigenspace with lowest eigenvalue for the
  {\color{myorange}magnetic Laplacian} $\overline{\partial}^*\overline{\partial}$ with
constant (large) field.
}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Toeplitz operators}
  \begin{itemize}
    \item Given $f:\C^n\to \R$ (say: $f:z\mapsto |z|^2$), quantize $f$ by setting
  \[
    \forall u,v \in B_N, \qquad \qquad \langle
    u,T_N(f)v\rangle:=\int_{\C^n} f\overline{u}v.
  \]

  Equivalently, ${\color{myorange}T_N(f)=S_NfS_N}$.

  \item<2-> Quantum harmonic oscillator
    $T_N(|z|^2)=N^{-1}\partial z$, \par eigenstates are the
    {\color{myorange} monomials}.
  \item<3-> The definition works for any $f\in L^{\infty}(\C^n,\R)$
    and produces a family of bounded self-adjoint operators.
  \item<4> Whenever $f,g\in C^2_b(\C^n,\R)$,
    \[
      [T_N(f),T_N(g)]=iN^{-1}T_N(\{f,g\})+O(N^{-2}).
      \]
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Toeplitz operators versus $\Psi$DOs}
  \begin{itemize}
  \item The Bargmann transform $\mathcal{B}_N$ conjugates $B_N$ and
    $L^2(\R^n)$.
  \uncover<2->{\item It is related to the FBI
    transform.}
  \uncover<3->{
  \item With $g_N=(N/\pi)^ne^{-N|z|^2}$ one has
    \[
      \mathcal{B}_N^{-1}T_N(f)\mathcal{B}_N=Op_W^{N^{-1}}(f*g_N).
    \]\vspace{-2em}}
\uncover<4->{\item Formal equivalence between Toeplitz and $\Psi$DO
  calculus for $C^{\infty}$ symbols.}
\uncover<5>{\item Toeplitz quantization is formulated {\color{myorange} directly in
  phase space}, it works for {\color{myorange}$L^{\infty}$ symbols}
and it is {\color{myorange} positive}.}

  \end{itemize}
\end{frame}

\begin{frame}\frametitle{Generalized Bargmann spaces}
  \begin{itemize}
    \item Change the exponential weight in the definition of $B_N$:
  \[
    B_N^{\psi}=\left\{f\in L^2(\C^n), e^{-N\psi}f \text{ is
        holomorphic}\right\}.
  \]
  \item Associated magnetic Laplacian, field and metric depend on
    $\psi$: {\color{myorange}new symplectic form}
    $\omega_{\psi}=i\partial\overline{\partial}\psi$.

    \vfill
    
  \item<2-> Geometrical setting: compact K\"ahler manifold $M$.\\
    \begin{minipage}{0.37\linewidth}\begin{itemize}
    \item Symplectic form
    \item Complex structure
    \end{itemize}
\end{minipage}
\uncover{\begin{minipage}{0.52\linewidth}

$\biggl\}$ Compatibility condition
\end{minipage}}
\item<2-> Glue together pieces $B_N^{\psi}$: space $H_N(M)$ of
  holomorphic sections.


  \vfill
  
\item<3> Szeg\H{o} projector $S_N:L^2(M,L^{\otimes N})\to H_N(M)$,\par
  Toeplitz quantization $T_N(f)=S_NfS_N$.
  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Examples: spheres and tori}
  \begin{description}
  \item[The two-sphere] $H_N=\;$homogeneous degree $N$ polynomials on
    $\C^2$\par $T_N($coordinates in $\R^3)= \text{{\color{myorange}spin operators}:}$
    \[
      [T_N(x),T_N(y)]=\frac{i}{N}T_N(z)\quad
      \text{ \& cyclic permutations.}
    \]

    Spin systems as integral (Toeplitz) operators:
    \mycite{[Lieb 77]}
    \item<2>[The two-torus] $H_N=\;$Dirac combs.

      $N$ even: one can quantize the {\color{myorange}catmap}:
    \end{description}
   \uncover<2>{\begin{center}
        \includegraphics[width=0.47\linewidth]{../img/catmap.png}\hfill
        \begin{minipage}{.5\linewidth}
          $\rightsquigarrow$ Unitary map $C_N$ on $H_N$\\
         \vspace{5em}
       \end{minipage}
     \end{center}}

   \vspace{-2.5em}
   
   \uncover<2>{$(C_N)_{N}$ exhibits {\color{myorange}Quantum Ergodicity}
   \mycite{[Zelditch 97]} but not {\color{myorange}Unique Quantum
     Ergodicity} \mycite{[Faure-Nonnenmacher-De Bièvre 03]}}
   \end{frame}

\begin{frame}
  \frametitle{Semiclassical analysis of Szeg\H{o} kernels}
 On  $\C^n$, there holds $S_N(x,y)=\frac{N^d}{\pi^d}\Psi^{\otimes
      N}(x,y)$, where $\Psi$ is a fixed section over $M\times M$ and
    $\log |\Psi|\asymp -\dist(x,y)^2$.
   
    \uncover<2->{\begin{theorem}[\mycite{[Charles 2003]}]
        Suppose $M$ is {\color{myorange}smooth}. There exists a section
        $\Psi$ and a sequence of functions $(s_k)_{k\geq 0}$ on
        $M\times M$ such that
      \[
        S_N(x,y)=N^d\Psi^{\otimes
          N}(x,y)(s_0+N^{-1}s_1+\cdots)(x,y)+O(N^{-\infty}).
      \]
      Here, for some $c>0$,
      \[
         |\Psi(x,y)|\leq -c\dist(x,y)^2.
        \]
      \end{theorem}}
    \uncover<3>{Applications (by stationary phase): composition, inverse, localisation of
      eigenfunctions for smooth Toeplitz operators!}
  \end{frame}

  \begin{frame}
    \frametitle{Challenge I: large dimension}
    Physicists want to study {\color{myorange}large spin systems}:
    Toeplitz quantizations of polynomials on $(\mathbb{S}^2)^d$ as
    $d\to +\infty$.

    \vfill

    Trouble: calculus of smooth Toeplitz operators ill-behaved in this
    limit.

    \vfill

    Preliminary results \mycite{[Deleporte, 20]}: localisation inequality:
    \[
      T_N(f)u=\lambda u \implies |u(x)|\leq
      C\exp\left(-c\frac{\sqrt{N}}{\sqrt{d}}\dist(x,\{f\approx \lambda\})\right).\]

    \vfill

    \begin{itemize}
      \item We need $\exp(-cN\dist^2(...))$ to study the thermodynamic
        limit!
      \item Influence of the geometry of $\{f=\lambda\}$?
      \end{itemize}
    \end{frame}

  \begin{frame}
    \frametitle{Challenge II: analytic regularity}
    Physicists want {\color{myorange}tunnelling estimates}: from
    superpolynomial error $O(N^{-\infty})$ to exponential error
    $O(e^{-cN})$.

    \vfill

    This can only be reached in {\color{myorange}analytic regularity}.

    \vfill

    \uncover<2>{Recent advances:
      \begin{itemize}
      \item The Szeg\H{o} kernel is known up to $O(e^{-cN})$
        \mycite{[Deleporte 20, Rouby-Sjöstrand-V\~u Ng\d{o}c 19]}
      \item Calculus of Toeplitz operators \mycite{[Deleporte 20]},
        WKB quasimodes \mycite{[Deleporte 19+]}
      \end{itemize}

      \vfill

      We need more!
      \begin{itemize}
      \item Large dimension $\exp(-cN)$ estimates ?
      \item Spectrum of non-self-adjoint operators ?
      \end{itemize}
    }
  \end{frame}

  \begin{frame}
    \centering
    {\LARGE Thanks for your attention!}
  \end{frame}

\end{document}
    
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
