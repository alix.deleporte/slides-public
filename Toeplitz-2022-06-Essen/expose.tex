\documentclass[mathserif]{beamer}
\usetheme{Boadilla}
%\usepackage[francais]{babel}
\usepackage[utf8]{inputenc} % Uses the utf8 input encoding
\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs
\usepackage[style=authoryear,backend=biber]{biblatex}
\addbibresource{main.bib}
\usepackage{etoolbox}
\makeatletter
\patchcmd{\@@description}{\advance\beamer@descdefault by
  \labelsep}{\advance\beamer@descdefault by -1em}{}{}
\makeatother

\usepackage{calc}
\usepackage{xcolor}

%\AtBeginSection[]
%{
%\setbeamercolor{section in toc}{fg=alerted text.fg}
%\setbeamercolor{section in toc shaded}{bg=structure!20,fg=structure}
%\setbeamertemplate{section in toc shaded}[default][100]
%\begin{frame}<beamer>
%  \frametitle{Outline}
%  \tableofcontents[currentsection,hideallsubsections]
%\end{frame}
%}

\definecolor{myorange}{RGB}{180,90,0}

\definecolor{mygreen}{RGB}{70,140,0}

\newcommand{\mycite}[1]{{\color{mygreen} \small #1}}

\usepackage[nomath]{kpfonts}
\usepackage{eulervm}
%\usepackage{default}

\input{thm-config}
\title[Analytic Bergman kernels]{Semiclassical Bergman kernels in analytic regularity}
\author{Alix Deleporte}
\date{June 22, 2022}
\institute[LMO]{Laboratoire de Mathématiques d'Orsay\\Université Paris-Saclay}
\newcommand{\spline}{\hline}
\renewcommand{\arraystretch}{1.3}

\DeclareSourcemap{
  \maps[datatype=bibtex]{
    \map[overwrite=true]{
      \step[fieldsource=author,
            match=Deleporte,
            final]
      \step[fieldset=keywords, fieldvalue=Deleporte]
    }
  }
}
\begin{document}

\beamertemplatenavigationsymbolsempty

    \expandafter\def\expandafter\insertshorttitle\expandafter{%
       \insertshorttitle\hfill%}
       }%\insertframenumber}


\begin{frame}
	\titlepage
      \end{frame}

      \begin{frame}
        \frametitle{Outline}
        \begin{enumerate}
        \item Setting and results.
          \begin{itemize}
          \item Weighted holomorphic $L^2$ spaces and holomorphic
            sections.
          \item The results.
          \item Why you should care about the analytic case.
          \end{itemize}
        \item Analytic semiclassical analysis.
          \begin{itemize}
          \item Analytic stationary phase lemma.
          \item Spaces of analytic symbols.
          \item Pseudodifferential operators.
          \end{itemize}
        \item Proof strategies
          \begin{itemize}
          \item Direct examination of the symbol.
          \item Microlocal conjugation with pseudodifferential
            operators.
          \end{itemize}
        \end{enumerate}
      \end{frame}

      \begin{frame}
        \frametitle{Local setting}
        On $\Omega\subset \C^n$ open, let $\Phi:\Omega\to \R$ be
        {\color{myorange}plurisubharmonic}:
        \[
          \left[\frac{\partial^2\Phi(z)}{\partial z_n\partial
              \overline{z_m}}\right]_{n,m}>>0.
        \]
        Hilbert space of weighted holomorphic functions
        \[
          H_{k}=\{f\in
          L^2(\Omega,\C),\overline{\partial}_kf=\overline{\partial}[fe^{k\Phi}]=0\}.
        \]
        {\color{myorange}Bergman kernel}: integral kernel of the
        orthogonal projection
        \[
          B_{k}:L^2(\Omega)\to H_k.
        \]
        Question: what does $B_k$ look like as $k\to +\infty$ ?
      \end{frame}

      \begin{frame}
        \frametitle{Global setting}
        We are not intersted in what happens at the boundary $\partial
        \Omega$, instead we {\color{myorange}glue several pieces}
        of the local model onto a complex manifold.

        \vfill

        The local setting only depends on the Kähler form
        $\omega=i\partial\overline{\partial}\Phi$. Starting from
        $(M,\omega,J)$ Kähler, compact, we obtain:
        \[
          {\rm gluing}(H_k)=H_0(M,L^{\otimes k})\qquad \qquad
          \text{space of holomorphic sections},\]
        where $L$ is a positively curved (ample) $\C$-bundle.
      \end{frame}

      \begin{frame}
        \frametitle{Brief history}
        \begin{itemize}
        \item First example $\Phi(z)=|z|^2$ and applications to quantum mechanics:
          \mycite{[Bargmann 61]}.
        \item Spectral gap for
          $\overline{\partial}_k^*\overline{\partial}_k$:
          \mycite{[Kohn 1963, Hörmander 1968]}.
 %       \item $|B_k(x,y)|\leq Ck^d\exp(-c\sqrt{k}\dist(x,y))$ \mycite{[Christ 1991,
%      Delin 1998]}.
  \end{itemize}
  Asymptotic formulas near the diagonal:
  \begin{itemize}
    \item FIOs with complex phase \mycite{[Shiffman-Zelditch 2002,
        Charles 2003]}: remainder $O(k^{-\infty})$, following
      \mycite{[Boutet de Monvel-Sjöstrand 1975]}.
    \item Weighted estimates on $\overline{\partial}^*_k\overline{\partial}_k$
      \mycite{[Ma-Marinescu 2007, Ma-Marinsecu-Kordyukov 2019]}.
      \item Analytic calculus \mycite{[\underline{Deleporte 2018},
        Rouby-Vũ Ng\d{o}c-Sjöstrand 2018]}: if $M$ is
      {\color{myorange} real-analytic},
      the remainder is $O(e^{-ck})$ with $c>0$.
    \end{itemize}
  \end{frame}
  
      \begin{frame}
        \frametitle{Motivations}
        Many objects are more natural in analytic regularity (e.g. the
        holomorphic extension of $\Phi$ appears in the formula for $B_k$).

        \vfill

        Toeplitz quantization: exponential precision leads
        to optimal eigenfunction estimates.

        \vfill

        Applications to the geometry of Kähler manifolds.
      \end{frame}

      \begin{frame}
        \frametitle{Analytic stationary phase - I}
        Our approximate formula for the Bergman kernel takes the WKB form:
        \[
          B_k(x,y)\approx k^de^{k\Psi(x,y)}(s_0(x,y)+k^{-1}s_1(x,y)+\ldots).
        \]
        We can conjecture this from the Bargmann case $\Phi=|x|^2$,
        where
        \[
          B_k(x,y)=k^de^{k(-|x|^2-|y|^2+2x\cdot
            \overline{y})}\times 1.
          \]

        \vfill
        
        The first step is to understand asymptotic properties of
        integrals of the form
        \[
          \int e^{k\varphi(x)}a(x)\dd x
        \]
        when $\varphi$ and $a$ are real-analytic.
      \end{frame}

      \begin{frame}
        \frametitle{Analytic stationary phase - II}
        Standard 1D example: $\varphi(x)=-\frac{x^2}{4}$. Usual stationary
        phase/saddle-point theorem
        tells us that
        \begin{align*}
          \int e^{-\frac{kx^2}{4}}a(x)\dd x&=exp(k^{-1} \Delta)a(0)\\
          &=\frac{1}{\sqrt{\pi
              k}}\sum_{j=0}^N\frac{\Delta^ja(0)}{k^jj!}+O(k^{-N-\frac 32}).
        \end{align*}
        Size of the $j$-th term if $a$ is real-analytic, with
        convergence radius $\rho$:
        \[
          \left|\frac{\Delta^ja(0)}{k^jj!}\right|\leq
          \frac{1}{k^jj!}\|a\|_{C^{2j}}\leq
          \frac{(2j)!}{\rho^{2j}k^jj!}\leq \frac{j!}{(2k\rho^2)^j}.
          \]
        \end{frame}
        \begin{frame}\frametitle{Analytic stationary phase - III}
          Size of the $j$-th term if $a$ is real-analytic, with
        convergence radius $\rho$:
        \[
          \left|\frac{\Delta^ja(0)}{k^jj!}\right|\leq
          \frac{1}{k^jj!}\|a\|_{C^{2j}}\leq
          \frac{(2j)!}{\rho^{2j}k^jj!}\leq \frac{j!}{(2k\rho^2)^j}.
        \]

        \vfill
        
        The rhs series does not converge!! The smallest term is at
        \[
          j\approx 2k\rho^2,
        \]
        and the size of this term, by Stirling formula, is:
        \[
          \frac{(2k\rho^2)!}{(2k\rho^2)^{2k\rho^2}}\sim \sqrt{4\pi k\rho^2}e^{-2
            k \rho^2}.
          \]
      \end{frame}

      \begin{frame}
        \frametitle{Analytic stationary phase - IIII}
       We obtain analytic stationary phase by
       {\color{myorange}optimisation of the term of the expansion}:
       for some $\alpha>0,\beta>0$, one has
        \[
          \int e^{-\frac{kx^2}{4}}a(x)\dd x=\frac{1}{\sqrt{\pi
              k}}\sum_{j=0}^{\alpha
            k}\frac{\Delta^ja(0)}{k^jj!}+O(e^{-\beta k}).
        \]

        This result can be generalised: the output of the stationary
        phase involves an {\color{myorange}analytic symbol}, up to an
        {\color{myorange}exponentially small} error.
      \end{frame}

      \begin{frame}
        \frametitle{Analytic symbols}

        \begin{defn}
          A function $a\in \R^n_x\times[0,1]_h\to \C$ is an
          analytic symbol when $a$ is smooth and its Borel transform
          \[
            \mathcal{B}a:(x;h)\mapsto \sum_{j=0}^{+\infty}\frac{\partial_h^ja(x;0)h^j}{j!^2}\]
          sums into a real-analytic function near $\{h=0\}$.
        \end{defn}
        In practice, $a=\sum h^ja_j(x)$ is a formal
        series satisfying
        \[
          \|a_j\|_{C^n}\leq C\frac{j!n!}{\rho^jR^n} \forall j,n.
        \]
        Banach spaces of analytic functions $\rightsquigarrow$ Banach
        spaces of symbols.
      \end{frame}

      \begin{frame}
        \frametitle{Practical example: pseudodifferential operators}
        Given $a:\R^{2d}\to \R$, we associate the self-adjoint operator $Op_h(a)$
        with kernel
        \[
          Op_h(a)(x,y)=\frac{1}{(2\pi h)^d}\int e^{i\frac{(x-y)\cdot
              \xi}{h}}a(\tfrac{x+y}{2},\xi)\dd \xi.
        \]
        \begin{itemize}
          \item They generalise differential operators (case where $a$ is a
            polynomial in $\xi$)
          \item Stable by composition, good description mod $O(h^N)$
            for all $N$.
            \item Stable by inverse, good description mod $O(h^N)$ for all
              $N$.
            \item It's a {\color{myorange}quantization}:
              $[Op_h(a),Op_h(b)]\approx ih Op_h(\{a,b\})$.
            \end{itemize}
          \end{frame}

      \begin{frame}
        \frametitle{Analytic symbol spaces for $\Psi$DOs}
        
        \begin{theorem}[\mycite{[Boutet-Krée 67, Sjöstrand 82]}]There
          are Banach norms of analytic symbols $\|\cdot\|$ in which
          the Moyal product is continuous: if $a$, $b$, $c$
          satisfy
          \[
            Op_{h}(a)Op_{h}(b)=Op_h(c),
          \]
          then
          \[
            \|c\|\leq C\|a\|\|b\|.
            \]
          \end{theorem}
          Proof: by hand, counting derivatives in the formula
            \[
              c(x,\xi;h)=\sum_{j}\frac{(ih)^j}{j!}(\nabla_x^ja\cdot
              \nabla_\xi^jb-\nabla_\xi^ja\cdot
              \nabla_x^jb).
              \]
      \end{frame}

      \begin{frame}
        \frametitle{My first proof in analytic microlocal analysis}
        Problem: prove that, if $a$ is bounded away from
        $0$, then $Op(a)$ has an inverse mod $O(e^{-ch^{-1}})$.

        \vfill

        \uncover<2->{Usual proof for $O(h^N)$: prove that
          \[Op_h(a)Op_h(a^{-1})=Op_h(1-h r),\] then correct $a^{-1}$ by
          induction.
        Are the coefficients in this induction bounded as analytic
        symbols? Very hard to prove.}

        \vfill

        \uncover<3>{New idea: Since $1+hr$ is close to $1$, use the
          Banach algebra norm to invert it with the convergent series
          \[
            Op_h(1-hr)^{-1}=1+Op_h(hr)+(Op_h(hr))^2+(Op_h(hr))^3+...
            \]}
      \end{frame}

      \begin{frame}
        \frametitle{The Bergman kernel}
        We wish to prove the following theorem.

        \begin{theorem}[\mycite{[Rouby-Sjöstrand-Vũ Ng\d{o}c 18,
            Deleporte 18]}]Let $\Phi:\C^n\to \R$ be plurisubharmonic and
          real-analytic.

          Let $\psi:\C^n\times \C^n\to \C$ be the
          holomorphic extension of $\Phi$.

          Then there exists an
          analytic symbol $s$ such that
          \[
            B_k(x,y)=k^de^{k(2\psi(x,y)-\Phi(x)-\Phi(y))}s(x,y;k^{-1})+O(e^{-\beta
              k}).
            \]
          \end{theorem}
        \end{frame}
        \begin{frame}
          \frametitle{Toeplitz operators}
          Operators with similar kernels are stable under composition:
          with
          \[
            T_k(a)(x,y)=k^de^{k(2\psi(x,y)-\Phi(x)-\Phi(y))}a(x,y;k^{-1}),
          \]
          then $T_k(a)T_k(b)$ is of the form $T_k(c)$, by stationary phase. These
          {\color{myorange}Toeplitz operators} are also a
          quantization, like $\Psi$DOs.


          \vfill
          
          What we need to prove: that there exists a unit modulo
          exponentially small error.
        \end{frame}

        \begin{frame}
          \frametitle{Complex Pseudodifferential operators}
          Path used in \mycite{[Rouby-Sjöstrand-Vũ Ng\d{o}c]}: use {\color{myorange}complex $\Psi$DOs}.

          Given a contour $\R^d\ni x\mapsto \Gamma(x)\subset \C^d$,
          define
          \[
            Op_{\Gamma,h}(a)(x,y)=\frac{1}{(2\pi
              h)^d}\int_{\Gamma(\frac{x+y}{2})}e^{i\frac{(x-y)\cdot\xi}{h}}a(\tfrac{x+y}{2},\xi)\dd \xi.\]
          \begin{prop}
            There is a contour $\Gamma$ and, for
            every analytic symbol $a$, an analytic symbol $b$, such that
            \[
              T_k(a)=Op_{\Gamma,k^{-1}}(b).
            \]
            Moreover, the map $a\mapsto b$ is invertible and
            continuous between two analytic symbol spaces.
          \end{prop}
          End of the proof: since $a\mapsto b$ is invertible, we obtain $s$ as the preimage of $b=1$.
        \end{frame}

        \begin{frame}
          \frametitle{Direct proof}
          Path used in \mycite{[Deleporte 18]}: we know from the smooth
          case that
          \[
            T_k(a)T_k(b)=T_k\left(\sum_{j\geq 0}k^{-j}C_j(a,b)\right).
          \]
          Here $C_j(a,b)$ involves derivatives 
          {\color{myorange}up to degree $j$ in $a$ and degree $j$ in
            $b$}.

          \vfill

          So we can reproduce the proof of the Banach algebra property
          (with the same count of derivatives).
        \end{frame}

        \begin{frame}
          \frametitle{What's next}
          \begin{itemize}
            \item
          Also in \mycite{[Deleporte 18]}: exponential concentration
          of eigenfunctions of self-adjoint $T_k(a)$.
        \item Shorter proofs for the Bergman kernel by Charles, Hezari
          et al., Deleporte-Hitrik-Sjöstrand.
        \item In progress (with S. Zelditch): understanding the
          Mabuchi space
          of Kähler structures using operators in WKB form.
        \item In progress (with M. Hitrik): the Szeg\H{o} kernel at
          the boundary of a strictly pseudoconvex analytic open set.
        \item Spectral theory for Non-Self-Adjoint Toeplitz operators.
        \end{itemize}
      \end{frame}
\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
