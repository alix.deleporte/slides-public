\documentclass[mathserif]{beamer}
\usetheme{Boadilla}
%\usepackage[francais]{babel}
\usepackage[utf8]{inputenc} % Uses the utf8 input encoding
\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs
\usepackage[style=authoryear,backend=biber]{biblatex}
\usepackage{dsfont}
\addbibresource{main.bib}
\usepackage{etoolbox}
\usepackage{stackrel}
\usepackage{esint}
\makeatletter
\patchcmd{\@@description}{\advance\beamer@descdefault by
  \labelsep}{\advance\beamer@descdefault by -1em}{}{}
\makeatother

\usepackage{calc}
\usepackage{xcolor}



\definecolor{myorange}{RGB}{180,90,0}

\definecolor{mygreen}{RGB}{70,140,0}

\newcommand{\mycite}[1]{{\color{mygreen} \small #1}}

\usepackage[nomath]{kpfonts}
\usepackage{eulervm}
%\usepackage{default}

\input{thm-config}
\title[DPPs and spectral projectors]{Determinantal point processes and semiclassical spectral projectors}
\author{Alix Deleporte}
\date{March 25, 2022}
\institute[LMO]{Laboratoire de Mathématiques d'Orsay\\Université Paris-Saclay}
\newcommand{\spline}{\hline}
\renewcommand{\arraystretch}{1.3}

\DeclareSourcemap{
  \maps[datatype=bibtex]{
    \map[overwrite=true]{
      \step[fieldsource=author,
            match=Deleporte,
            final]
      \step[fieldset=keywords, fieldvalue=Deleporte]
    }
  }
}
\begin{document}

\beamertemplatenavigationsymbolsempty

    \expandafter\def\expandafter\insertshorttitle\expandafter{%
       \insertshorttitle\hfill%}
       }%\insertframenumber}


\begin{frame}
	\titlepage
      \end{frame}


      \begin{frame}
        \frametitle{Outline}
        \begin{enumerate}
        \item Determinantal point processes
          % \begin{itemize}
          % \item Free fermions and Slater determinants
          % \item Determinantal point processes
          % \item Random matrices
          % \end{itemize}
        % \item Determinantal processes for the Bergman kernel
        %   \begin{itemize}
        %   \item Linear statistics
        %   \item Entropy
        %   \item Fekete points
        %   \end{itemize}
        \item Weyl law and microscopic limit
        \item Macroscopic fluctuations
        \end{enumerate}
      \end{frame}
      

      \section{Determinantal point processes}

      \begin{frame}
        \frametitle{Free fermions}
        Quantum state of {\color{myorange}one particle}: element of $L^2(\R^d)$ (or any
        Hilbert space $\mathcal{H}$).
        
        \vfill
        
        Quantum state of {\color{myorange}N identical fermions}:
        element of
        \[
          [L^2(\R^d)]^{\otimes N}_{\text{antisym}}:=\{f\in
          L^2,f(x_{\sigma(1)},\ldots,x_{\sigma(n)})=\text{sign}(\sigma)f(x_1,\ldots,x_n)\}.
        \]

        \vfill

        Pauli : ``Particles cannot be at the same place'' ($f(x,x,\ldots)=0$)
      \end{frame}

      \begin{frame}
        \frametitle{Slater determinants}
        Model: $N$ identical fermions subject to the same one-particle
        Hamiltonian $H$ (self-adjoint operator on $\mathcal{H}$,
        bounded from below).

        \vfill

        Energy minimised when the fermions {\color{myorange} fill the $N$ lowest
        levels of $H$}: \uncover<2>{with a spectral decomposition
        \[
          H\phi_j=\lambda_j\phi_j\qquad \qquad \lambda_j\nearrow,
        \]
        the state of minimal energy is
        \begin{align*}
          \Psi:(x_1,\ldots,x_n)\mapsto
          &\frac{1}{\sqrt{N!}}\sum_{\sigma\in
          S_N}\phi_1(x_{\sigma(1)})\ldots \phi_N(x_{\sigma(N)}){\rm sign}(\sigma)\\
          =&\frac{1}{\sqrt{N!}}\det[\phi_j(x_k)]_{\substack{1\leq j
             \leq N\\1\leq k\leq N}}
        \end{align*}}
      \end{frame}

      \begin{frame}
        \frametitle{Example: Aufbau principle}
        One-particle Hamiltonian: $-\hbar^2\Delta-\frac{1}{|x|}$ on $\R^3$:

        \begin{center}
          \includegraphics[width=0.5\linewidth]{../img/Aufbau.jpg}
        \end{center}
        
        Fermi energy $\mu$: everything is filled below $\mu$.
      \end{frame}
      
      \begin{frame}
        \frametitle{Determinantal point processes}
        Joint {\color{myorange}probability distribution} of the $N$
      particles: density
      \[
        |\Psi(x_1,\ldots,x_N)|^2=\frac{1}{N!}\det[\Pi(x_j,x_k)]_{\substack{1\leq j
            \leq N\\1\leq k\leq N}}
      \]
      where $\Pi$ is the kernel of the {\color{myorange}spectral
        projector}
      \[
        \Pi(x,y)=\sum_{\lambda_j\leq \mu}\phi_j(x)\overline{\phi_j(y)}=\mathds{1}_{(-\infty,\mu]}(H)(x,y).
      \]

      \vfill

      \uncover<2->{
      Marginals: for every $1\leq n\leq N$, the joint probability
      distribution of the particles $1,\ldots,n$ is
      \[\frac{(N-n)!}{N!}\det[\Pi(x_j,x_k)]_{\substack{1\leq j
            \leq n\\1\leq k\leq n}}
      \]}
  \end{frame}

  \begin{frame}\frametitle{One-point statistics}
      \[
        C^0(\R^d,\R)\ni u \mapsto
        \mathbf{X}(u)=u(x_1)+u(x_2)+\ldots+u(x_N)
      \]
      The knowledge of $\mathbf{X}(u)$ for all $u$ fully determine the
      process (one-point statistics are enough).

      \vfill

      \begin{align*}
        \mathbb{E}[\mathbf{X}(u)]&={\rm Tr}(\Pi u)\\
        {\rm Var}[\mathbf{X}(u)]&=-{\rm Tr}([\Pi,u]^2)\\
        \mathbb{E}[e^{-\mathbf{X}(u)}]&=\det(I+(1-e^{-u})\Pi)
      \end{align*}
      \end{frame}
      
      \begin{frame}
        \frametitle{Random matrices}
        {\color{myorange}GUE model}: Hermitian $N\times N$ matrix:
        \begin{itemize}
        \item Diagonal entries $\sim
          \frac{1}{2\sqrt{N}}\mathcal{N}_\R(0,1)$
        \item Off-diagonal entries $\sim \frac{1}{2\sqrt{2N}}\mathcal{N}_\C(0,1)$
        \end{itemize}

        Joint law of the eigenvalues = DPP on $\R$ associated with
        \[
          H=-\frac{1}{2N^2}\Delta+\frac{x^2}{2} \qquad \mu = 1.
        \]

        \begin{center}
          \includegraphics[width=0.5\linewidth]{../img/Wigner_semicircle.png}\\
          {\tiny histogram of the eigenvalues for a GUE
            realisation. $N=3000$.}
        \end{center}
        
        \end{frame}

        \begin{frame}
          \frametitle{Typical questions}
          Given a family of spectral projectors
          \[
            \Pi_{\hbar}=\mathds{1}_{(-\infty,\mu]}(-\hbar^2\Delta+V),
          \]
          with $V$ fixed, $\mu$ fixed, $\hbar\to 0$:
          \begin{description}
          \item[\color{myorange}Global behaviour] Limit distribution?
            Scale and type of fluctuations?
          \item[\color{myorange}Local behaviour] Convergence of the
            DPP zoomed at scale $\hbar$?
          \item[\color{myorange}Edge] What happens at the boundary of
            the support of the limit distribution ?

            \vfill

            
          \item[\color{myorange}Projection kernel] How to pass from
            kernel asymptotics to these probabilistic results?
          \end{description}
        \end{frame}

        \begin{frame}
          \frametitle{Literature}
          On this problem:
          \begin{itemize}
          \item Harmonic oscillator example: well known.
            \item \mycite{[Bloch-Dalibard-Zwerger 08,
                Giorgini-Pitaevskii-Stringari 08]} Experimental
              results in $\R^3$.
          \item \mycite{[Le Doussal et al., 15$\rightarrow$21]} many
            non-rigorous theoretical investigations.
          \item \mycite{[D.-Lambert 21]} General Schrödinger operators.
          \end{itemize}
          
          \vfill

          On Hall-type free fermions (``Ginibre universality class'') :
          \begin{itemize}
          \item \mycite{[Berman 13-14, Klevtsov 14, Charles-Estienne 18,
              Hedenmalm et al. 11$\rightarrow$20]}
            From Bergman projector asymptotics to probabilistic results. 
          \end{itemize}
        \end{frame}

        \AtBeginSection[]
{
\setbeamercolor{section in toc}{fg=alerted text.fg}
\setbeamercolor{section in toc shaded}{bg=structure!20,fg=structure}
\setbeamertemplate{section in toc shaded}[default][100]
\begin{frame}<beamer>
  \frametitle{Outline}
  \tableofcontents[currentsection,hideallsubsections]
\end{frame}
}

      \section{Weyl law and microscopic limit}

      % \begin{frame}
      %   \frametitle{Setup}
      %   \(
      %   \left\{\text{
      %     \begin{tabular}{@{\textbullet\enspace}l}
      %       Compact complex manifold $M$ \\
      %       Hermitian line bundle $L\to M$\\
      %       Volume form $\mu$ on $M$.
      %     \end{tabular}}
      %   \right.
      %   \)

      %   {\color{myorange}Bergman kernel} $\Pi_k$ projecting on holomorphic
      %   sections of $L^{\otimes k}$: 
      %   kernel of $\Delta_k=\overline{\partial}^*\overline{\partial}$
      %   acting on sections of $L^{\otimes k}$.

      %   \vfill

      %   Locally, in the {\color{myorange}bulk} $\{{\rm curv}(L)>0\}$, {\color{myorange}spectral gap} for $\Delta_k$.

      %   \vfill

      %   \uncover<2>{
      %   Smooth case: known asymptotics for the kernel of
      %   $\Pi_k$ in the bulk. One has
      %   \[
      %     N\asymp k^{-d}
      %   \]
      %   as soon as the bulk is non-empty.}
      % \end{frame}

      % \begin{frame}
      %   \frametitle{Convergence of linear one-particle statistics}
      %   \begin{theorem}[\mycite{[Berman 08-16]}]
      %     Suppose $\mu\in C^0$ and $\phi\in C^{1,1}$. Let $u\in
      %     C^0(M,\R)$. Then, as $k\to +\infty$,
      %     \[
      %       \frac{1}{N}\mathbf{X}(u)\to \int_X u \dd \nu_{\rm eq}(\mu,\phi)
      %     \]
      %     in probability, for some equilibrium measure $\nu_{\rm eq}$
      %     supported where $L$ is positively curved.

      %     More
      %     precisely, there exists $C>0$ such that, for every $\epsilon>0$,
      %     \[
      %       \mathbb{P}\left[\left|\tfrac{1}{N}\mathbf{X}(u)- \int_X u
      %           \dd \nu\right|>\epsilon\right]\leq
      %       \frac{C}{\epsilon k^d}.
      %       \]
      %   \end{theorem}
      % \end{frame}

      % \begin{frame}
      %   \frametitle{Fluctuations of linear one-particle statistics}
      %   \begin{theorem}[\mycite{[Berman 08-16]}]
      %     Suppose $\mu\in C^0$ and $\phi\in C^{1,1}$. Let $u:M\to \R$
      %     Lipschitz-continuous, {\color{myorange}supported inside the
      %       bulk}. Then
      %     \[
      %       Var[\mathbf{X}(u)]\sim k^{d-1}\int_X \partial u
      %       \overline{\partial}u (\partial
      %       \overline{\partial}\phi)^{\wedge n-1}.
      %     \]
      %     More precisely, the rescaled {\color{myorange}moment
      %       generating function}
      %     \[
      %       t\mapsto
      %       \mathbb{E}\left[\exp\left(tk^{-(d-1)/2}(\mathbf{X}[u]-\mathbb{E}[\mathbf{X}[u]])\right)\right]
      %         \]
      %         converges to that of a Gaussian with variance
      %         $\int_X\partial u \overline{\partial}u (\partial
      %       \overline{\partial}\phi)^{\wedge n-1}.$
      %   \end{theorem}
      % \end{frame}

      % \begin{frame}
      %   \frametitle{Scheme of proof}
      %   \begin{enumerate}
      %   \item Kernel asymptotics in the bulk
      %   \item Formulas for expectation and variance
      %     \begin{align*}
      %       \mathbb{E}[\mathbf{X}(u)]&=\int \Pi_{\hbar}(x,x)u(x)\dd \mu(x)\\
      %       {\rm Var}[\mathbf{X}(u)]&=\int
      %                                 |\Pi_{\hbar}(x,y)|^2|u(x)-u(y)|^2\dd
      %                                 \mu(x)\dd \mu(y).
      %     \end{align*}
      %   \item Bienaymé-Tschebyscheff for LLN, DPP magic for CLT
      %   \end{enumerate}
      % \end{frame}

      % \begin{frame}
      %   \frametitle{Entanglement entropy}
      %   Behaviour of {\color{myorange}the number of particles in
      %     a box}, $\mathbf{X}[\mathds{1}_\Omega]$?

      %   Answer: sum of independent Bernoulli's whose parameters are
      %   {\color{myorange}eigenvalues of the Toeplitz operator}
      %   $\Pi_k\mathds{1}_{\Omega}\Pi_k$.

      %   % de la place pour un dessin

      %   \uncover<2>{Physically relevant: entropy
      %   \[
      %     {\rm Ent}(\Omega)=\sum_{\lambda_j\in {\rm
      %         Sp}(\Pi_k\mathds{1}_{\Omega}\Pi_k)}-\lambda_j\log(\lambda_j)-(1-\lambda_j)\log(1-\lambda_j)
      %   \]

      %   \begin{theorem}[\mycite{[Charles--Estienne 18]}] Assume $L$ is
      %     positive everywhere, smooth, and $\mu=(\partial
      %     \overline{\partial}\phi)^{\wedge d}$. Then, as $k\to
      %     +\infty$,
      %     \begin{align*}
      %       {\rm Ent}(\Omega)&\sim C_1k^{n-\frac 12}{\rm Vol}(\partial
      %                          \Omega)\\
      %       {\rm Var}(\mathbf{X}[\mathds{1}_{\Omega}])&=C_2k^{n-\frac
      %                                                   12}{\rm
      %                                                   Vol}(\partial
      %                                                   \Omega)
      %     \end{align*}
      %   \end{theorem}
      %   One could prove LLN+CLT in this context.}
      % \end{frame}

      % \section{Without gap: Semiclassical Schrödinger operators}

      \begin{frame}
        \frametitle{Context}
        Schrödinger operator on $L^2(\R^n)$
        \[
          H_{\hbar}=-\hbar^2\Delta+V.
        \]

        Fermi energy $\mu>\min(V)$.
        Projector:
        \[
          \Pi_{\hbar}=\mathds{1}_{(-\infty,\mu]}(H_{\hbar}).
        \]
        Heuristics: $\Pi_{\hbar}$, near $x_0\in \R^n$,
        looks like
        $\mathds{1}_{(-\infty,\mu]}(-\hbar^2\Delta+V(x_0))$.

        \vfill

        
        Perturbative method hard to apply: {\color{myorange}no
          spectral gap at $\mu$}. We need to use
        {\color{myorange}semiclassical analysis}.
      \end{frame}

      \begin{frame}
        \frametitle{Projector kernel asymptotics}
        Technical hypotheses: {\color{myorange} $V\in L^1_{\rm loc}$, it
        is $C^{\infty}$ on the compact $\{V\leq
          \mu+\epsilon\}$.}

        Example: repulsive Coulomb is OK, attractive Coulomb is not.
        \begin{theorem}[\mycite{[D.-Lambert 21]}]
          Let $x_0\in \{V<\mu\}$. Then, locally uniformly in $(x,y)$,
          \[
            \left|\hbar^n\Pi_{\hbar}(x_0+\hbar x, x_0+\hbar
              y)-\mathds{1}_{-\Delta+V(x_0)\leq \mu}(x,y)\right|\leq C\hbar.
          \]
          Derivatives with respect to $x,y$ at any order also converge.
          \end{theorem}
          Nothing more precise: $\Pi_{\hbar}$ oscillates at
          scale $\hbar$.

          \vfill

          \uncover<2>{
          Well-known kernel with
          polynomial decay.
          \[
            \mathds{1}_{-\Delta\leq 1}(x,y)=C_n\frac{J_{\frac
                n2}(\dist(x,y))}{\dist(x,y)^{\frac n2}}.
          \]}
        \end{frame}

        \begin{frame}
          \frametitle{The Fourier Tauberian method}
          Follow Carleman, Levitan, Hörmander, Chazarain, Helffer-Robert: write
          \[
            \mathds{1}_{(-\infty,\mu]}(H_{\hbar})=\frac{1}{2\pi\hbar}\int
            e^{i\frac{tH_{\hbar}}{\hbar}}e^{-it\lambda}\mathds{1}_{(-\infty,\mu]}(\lambda)\dd
            \lambda\dd t.
          \]
          The operator $e^{i\frac{tH_{\hbar}}{\hbar}}$ is the solution
          of the quantum evolution
          \[
            -i\hbar \partial_t\psi=H_{\hbar}\psi
          \]
          which should mimic classical evolution:
          {\color{myorange}quantum-classical correspondence}.

        \end{frame}

        
        \begin{frame}
          \frametitle{The Fourier Tauberian method, contd.}
          Propagator $e^{it\frac{H_{\hbar}}{\hbar}}$ known
          for {\color{myorange}short time} $|t|<c$:
          $\exists \varphi$ and $a$ such that
          \[
            e^{i\frac{tH_{\hbar}}{\hbar}}(x,y)=
            \hbar^{-n}\int
            e^{\frac{i}{\hbar}(\varphi(t,x,\xi)-\langle
              y,\xi\rangle)}a(t,x,y,\xi;\hbar)\dd \xi+O_{{\rm
                Tr}_{{\rm loc}}}(\hbar^{\infty}).
          \]
          Strategy of proof:
          \begin{enumerate}
          \item Truncate the Fourier inversion formula to $|t|<c$ and
            compute the limit using stationary phase
            method.
            \item 
          Estimate the remainder
          \[
            \frac{1}{2\pi\hbar}\int
            e^{i\frac{tH_{\hbar}}{\hbar}}e^{-it\lambda}f(\lambda){\color{myorange}\mathds{1}_{|t|>c}}\dd
            \lambda
          \]
          using a Tauberian theorem.
        \end{enumerate}
        \begin{theorem}[Weyl Law, \mycite{[Helffer-Robert 81]}]
          Under a non-degeneracy assumption,
          \[
            N=(2\pi\hbar)^{-n}\int_{\R^{2n}}\mathds{1}(V(x)+|\xi|^2<\mu)\dd x
            \dd \xi+O(\hbar^{-n+1}).
            \]
        \end{theorem}

      \end{frame}
      \begin{frame}
        \frametitle{Projector kernel asymptotics at the edge}
         \begin{theorem}[\mycite{[D.-Lambert 21]}]
        Let $x_0\in \{V=\mu\}$. Suppose that
        {\color{myorange}$\nabla V(x_0)\neq 0$}. Then, locally uniformly,
        \[
          |\hbar^{\frac{2n}{3}}\Pi_{\hbar}(x_0+\hbar^{\frac
            23}x,x_0+\hbar^{\frac 23}y)-\mathds{1}_{-\Delta+\langle \nabla
            V(x_0),\cdot\rangle \leq 0}|\leq C\hbar^{\frac 13}.
        \]
      \end{theorem}
      We obtain another well-known limit object, the
        {\color{myorange}Airy kernel}.

        \vfill

        \uncover<2>{
        Differences with the bulk case:
        \begin{itemize}
        \item Oscillatory integral with degenerate critical point
        \item No convergence of derivatives
        \end{itemize}}
    \end{frame}

      \begin{frame}
        \frametitle{Microscopic limit}
        \begin{theorem}[\mycite{[D.-Lambert 21]}]
          \begin{itemize}
          \item After rescaling by $\hbar$ near a point in the bulk,
            the point process converges weakly (see next slide) to the Bessel DPP.
          \item After rescaling by $\hbar^{\frac 23}$ near a point at
            the edge, the point process converges weakly to the Airy DPP.
          \end{itemize}
        \end{theorem}
          \vfill
          
          At these ``microscopic'' scales, we know
          {\color{myorange}everything}: typical $k$-point
          configurations, etc
        \end{frame}

        \begin{frame}
          \frametitle{Weak convergence of DPPs}
          Point process $\mathbf{X}$ on $\R^n$ $\rightsquigarrow$
          {\color{myorange} Laplace transform}
          \[
            C^{\infty}_c(\R^n,\R^+)\ni f \mapsto
            \psi_{\mathbf{X}}(f):=\mathbb{E}\left[\exp(-\mathbf{X}(f))\right].
          \]

          \uncover<2->{
          In our case (associated with a spectral projector on $N$
          particles), recall
          \[
            \psi_{\mathbf{X}}(f)=\det[I+(1-e^{-f})\Pi_{\hbar}].
          \]

          We are going to prove convergence of the Laplace transform
          for $f$ not necessarily purely imaginary.
        }

          % \uncover<3>{\begin{defn}
          %   A sequence of DPPs $\mathbf{X}_n$ is said to
          %   {\color{myorange}converge weakly} to a DPP $\mathbf{X}$
          %   when, for all $f\in C^{\infty}_c(\R^n,\R)$, as $n\to +\infty$,
          %   \[
          %     \psi_{\mathbf{X}_n}(if)\to \psi_{\mathbf{X}}(if).
          %     \]
          %  \end{defn}
          %}
      \end{frame}

      \begin{frame}
        \frametitle{From kernel convergence to DPP weak convergence}
        We know that $\Pi_{\hbar}$ converges pointwise to some limit
        $\Pi$ at the relevant scale.

        \begin{prop}
          Let $g\in C^{\infty}_c(\R^n,\R)$ and $A,B$ be two locally trace
          class operators. Then
          \[
            |\det(I+gA)-\det(I+gB)|\leq
            \|g(A-B)\|_{\rm Tr}e^{1+\|gA\|_{\rm Tr}+\|gB\|_{\rm Tr}}.
            \]
          \end{prop}

          We really need trace norm estimates; sometimes they can be hard to relate
          to the kernel. In our case, we can use a theorem by Grümm.

          \vfill

          \phantom{toto}
        \end{frame}

        \section{Macroscopic fluctuations}
        
      \begin{frame}
        \frametitle{Law of large numbers}
        \begin{theorem}[\mycite{[D.-Lambert 21]}]
          \[\text{Let}\qquad \qquad \qquad \rho:x\mapsto \frac{(\mu-V(x))_+^{\frac n2}}{\int
              (\mu-V(y))_+^{\frac n2}\dd y}.
            \]Then for every
          {\color{myorange}$u\in L^{\infty}$} one has convergence in probability
\[
          \frac{1}{N}\mathbf{X}[u]\to \int \rho (x)u(x)\dd x
          \]
          and a uniform concentration inequality for {\color{myorange}
            $u\in {\rm Lip}$}:
          \[
            \mathbb{P}\left[\dist_{{\rm
                  Lip}^*}\left(\frac{1}{N}\mathbf{X},\rho\right)\geq \epsilon\right]\leq
          C(\epsilon)\exp(-cN\epsilon^2).
          \]
        \end{theorem}
        Use elementary, non-sharp, variance estimate:
        \[
          {\rm Var}[\mathbf{X}(u)]=-\tr([\Pi_{\hbar},f]^2)\leq
          2N\|f\|_{\infty}^2\leq C\hbar^{-n}\|f\|_{\infty}^2.
        \]
      \end{frame}

      \begin{frame}
        \frametitle{Size of commutator: heuristics}
        \[
          \tr([\Pi_{\hbar},e^{ig}][\Pi_{\hbar},e^{-ig}])=\tr(\Pi_{\hbar}(\Pi_{\hbar}-e^{ig}\Pi_{\hbar}e^{-ig})).
        \]
        \begin{center}
          \includegraphics[width=0.5\linewidth]{../img/size_commutator_proj.jpg}
        \end{center}

        Expected size: $\hbar^{-n}{\rm Vol}(\text{red
          part})\asymp \hbar^{-n+1}$.

        \vfill

        \uncover<2>{
        Explicit formula in the flat case:
        \[
          \tr([\mathds{1}_{-\hbar^2\Delta\leq 1},f]^2)\sim C_n\hbar^{-n+1}\|f\|_{H^{\frac
              12}}^2.
          \]}
      \end{frame}
      \begin{frame}
        \frametitle{Limit of commutator in the flat case}
        After conjugation by a Fourier transform,
        \[
          -\tr([\mathds{1}_{-\hbar^2\Delta\leq
            1},f]^2)=\frac{1}{(2\pi)^n}\int
          |\hat{f}(\xi)|^2|\mathds{1}_{|\zeta|<\hbar^{-1}}-\mathds{1}_{|\zeta+\xi|<\hbar^{-1}}|^2\dd
          \zeta \dd \xi.\]
        The integral over $\zeta$ is the area of a large ball minus a
        shifted large ball: for fixed $\xi$, as $\hbar\to 0$,
        \begin{align*}
          \int |\mathds{1}_{|\zeta|<\hbar^{-1}}-\mathds{1}_{|\zeta+\xi|<\hbar^{-1}}|^2\dd
          \zeta &=\hbar^{-n}{\rm Vol}(B_n(0,1)\setminus B_n(\hbar
          \xi,1))\\ &\sim \hbar^{-n+1}|\xi|{\rm Vol}(B_{n-1}(0,1)).
        \end{align*}
        We conclude by dominated or monotone convergence:
        \[
          -\tr([\mathds{1}_{-\hbar^2\Delta\leq
            1},f]^2)\sim \frac{{\rm
              Vol}(B_{n-1}(0,1))}{(2\pi)^n}\hbar^{-n+1}\int|\hat{f}(\xi)|^2|\xi|\dd
          \xi.
          \]
      \end{frame}

      \begin{frame}
        \frametitle{Size of commutator: results}
        \begin{theorem}[\mycite{[D.-Lambert 21]}]
          Let $f\in C^{\infty}_c(\{V<\mu\},\R)$. Then
          \[
            c(f)\hbar^{-n{\color{myorange}+2}}\leq -\tr([\Pi_{\hbar},f]^2)\leq
            C(f)\hbar^{-n{\color{myorange}+1}}.
          \]
          If $n=2$, then the constant $c$ can be taken arbitrarily large :\[-\tr([\Pi_{\hbar},f]^2)\to +\infty.\]
        
        In particular, {\color{myorange} CLT if $n\geq 2$}: for all $f\in C^{\infty}_c$,
        \[
          \frac{\mathbf{X}(f)-\mathbb{E}[\mathbf{X}(f)]}{\sqrt{{\rm
                Var}[\mathbf{X}(f)]}}\Rightarrow \mathcal{N}(0,1).
          \]
        \end{theorem}
        We conjecture that $c(f)\hbar^{-n+1}\leq -\tr([\Pi_\hbar,f]^2)$.
        % \vfill
        
        % \uncover<2>{
        %   \begin{itemize}
        %   \item 
        %     There are counterexamples to CLT in dimension $n=1$.
        %     \item Even if $n\geq 2$, we expect the variance to
        %       oscillate (like the kernel).
        %   \end{itemize}}
      \end{frame}

      \begin{frame}
        \frametitle{Mesoscopic scales}
        Description at intermediate scales $h^{\alpha}$, for
        $0<\alpha<1$? Given $x_0\in \{V<\mu\}$, let
        \[
          f_{\alpha}:x\mapsto f(\hbar^{-\alpha}(x-x_0)).
          \]
        \begin{theorem}[\mycite{[D.-Lambert 21]}]
          Let $f\in C^{\infty}_c(\R^n)$. Suppose
          {\color{myorange}$n\geq 2$} and let $x_0\in \{V<\mu\}$. Then, as $\hbar \to 0$,
          \begin{itemize}
          \item $\mathbb{E}[\mathbf{X}(f_{\alpha})]\sim
            C_1(x_0)\hbar^{(1-\alpha) n}\int f.$
          \item ${\rm Var}[\mathbf{X}(f_{\alpha})]\sim
            C_2(x_0)\hbar^{(1-\alpha)(n-1)}\|f\|^2_{H^{\frac
                12}}.$
          \item CLT.
          \end{itemize}
        \end{theorem}
      \end{frame}

      \begin{frame}
        \frametitle{Work in progress: counting statistics}
        What is the law of the number of particles in an open set $\mathbf{X}(\mathds{1}_{\Omega})$?

        \vfill

        Widom conjecture: $\log$ factor in variance
        \[
          {\rm Var}(\mathbf{X}(\mathds{1}_{\Omega}))\sim
          \hbar^{-n+1}{\color{myorange}\log(\hbar)}C(\Omega).
        \]
        \begin{itemize}
        \item Compatible with $H^{\frac 12}$ correlation heuristics.
        \item Implies CLT in all dimensions. 
        \end{itemize}
        Also relevant to physicists: other norms of
        $[\Pi_{\hbar},\mathds{1}_{\Omega}]$ like {\color{myorange}entropy}.
      \end{frame}

      \begin{frame}
        \frametitle{Other perspectives}
        \begin{itemize}
        \item Macroscopic limit process in 1D: Gaussian limit ? Discrete
          fluctuations ? Link with {\color{myorange}Szeg\H{o} limit formula}.
        \item Other local models? Hyperbolic points, boundary problems, Dirac-type
          operators, ... The hardest is {\color{myorange}macroscopic
            fluctuations}.
        \item Interacting fermions ? We leave the realm of DPPs.
        \end{itemize}
      \end{frame}

  \begin{frame}
    \frametitle{Thanks}
    \centering 
    {\Large Thanks for your attention!}
  \end{frame}

\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
