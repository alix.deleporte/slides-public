# Public slides repository

Welcome! You'll find here the slides for most of my research-related public interventions.

They're arranged into folders, each of them containing exactly one set of slides in PDF form, along with the LaTeX sources.

The most recent talks are in folders named with the convention "Topic-Year-Month-Venue". Older talks (in the appropriate folder) are named with the convention "Year-Month Venue".

If you find typos or mistakes, feel free to contact me at my current email address:

alix.deleporte@universite-paris-saclay.fr
